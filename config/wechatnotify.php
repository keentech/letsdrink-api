<?php
return [
	'default' => env('WEHCAT_NOTIFY_ENV','test'),
	'template' => [
		'production' => [
			'BUY_SUCCESS' 		=> 'tWopo8F00os4En481WteFM8Agn6WDjBFMrur9QQUeUE',
			'ORDER_DELIVERY' 	=> 'mIXMVR-JQLxT8oOkS9EyadRLeUaH-y5Mu69YhNFMF0k',
			'REFUND' 			=> 'trbprD2GshBfalEzMpLsGLTXVyJSwpXZpGlIZrmNbg0',
			'CONFIRM_RECEIPT'	=> 'tjuWO6LEYhsTQlzi4nwI0T7CWGZh5dabbyiZHYDT62M',
			'CARD_EXPIRE' 		=> 'AmvA2AuDbyCa9gIbzvo0owGQtV52BGfaqr7FqYewP-g',
			'GIFT_RECEIVED' 	=> 's6gFlYSOA2OEBdfndKAXoVL7FSZyEyVg7bIRaoKHkUg',
			'TRY_APPLY' 		=> '1dkpmnZBCEP3gb7rZGcovjNHqlYbyLhdBb9NFfoeJT0',
			'TRY_REPORT_SUBMIT' => 'J1O-s0LLItwYbuMZkebtsLkf3HbXmHgmYw76DFKRC-8',
            'KEFU_TEL'          => '15001005623',
            'SHANG_JIA'         => "乐饮 Let's Drink",
            'FAHUO'             => "乐饮 Let's Drink",
		],
		'test' 	=> [
			'BUY_SUCCESS' 		=> 'ydt9fYPq3ncj-zWTfHbjByu2Ke3GdiD1feBYstrJiOk',
			'ORDER_DELIVERY' 	=> '6TmT90p_O6zwikeA9t_bDT17kp9PniXCZBENGHB6viQ',
			'REFUND' 			=> 'QriZevU1z_r_cN2REG7EI6xim3g7pa_QM1VmIr7RV7Q',
			'CONFIRM_RECEIPT'	=> 'pIAQwIFo_4sMCSePjY0MMUf8x4moGYBA3EHGDlM4xcY',
			'CARD_EXPIRE' 		=> 'VW8Tsq7sAnrExO0tG1Qt3Im0y8VULnW_T4a4oRducAQ',
			'GIFT_RECEIVED' 	=> 'hpiGI5XRNjNLbS8-Xk6J35Vu8yjeElSdhaElUWkNaag',
			'TRY_APPLY' 		=> 'rz8HCBAZduXllhHmdeEbUNZ7FUiPG0RyMeuWRXnOZ9E',
			'TRY_REPORT_SUBMIT' => '5NifmLVtC87oE4xkKybPp_vdLZfkINmh96PFnNC_brk',
            'KEFU_TEL'          => '13693238613',
            'SHANG_JIA'         => "TEST乐饮 Let's Drink",
            'FAHUO'             => "TEST乐饮 Let's Drink",
        ],
	],
];