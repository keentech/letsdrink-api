<?php

namespace App\Model;

use App\BaseModel;
use App\Helpers\Helpers;

class WineChateau extends BaseModel
{
    protected $table ='wine_chateau';

    protected $fillable = [
        'chateauCode',
        'state',
        'produce_id',
        'user_id',
        'name',
        'chname',
        'weight',
        'online',
        'online_at',
        'offline_at',
        'address',
        'levelType',
        'website',
        'summary',
        'intro',
        'feature',
        'owner',
        'owner_intro',
        'area',
        'age',
        'soil',
        'harvest',
        'remark',
        'updater',
        'creator',
    ];

    //酒庄图片
    public function chateauPic()
    {
        return $this->belongsToMany('App\Model\Resource','sys_resource_table',
            'obj_id','resource_id')->wherePivot('obj_type','chateau');
    }
    //庄主图片
    public function chateauMasterPic()
    {
        return $this->belongsToMany('App\Model\Resource','sys_resource_table',
            'obj_id','resource_id')->wherePivot('obj_type','chateau_master');
    }
    //葡萄园图片
    public function chateauGraperyPic()
    {
        return $this->belongsToMany('App\Model\Resource','sys_resource_table',
            'obj_id','resource_id')->wherePivot('obj_type','chateau_grapery');
    }
    //葡萄
    public function chateauGrapery()
    {
        return $this->hasMany('App\Model\WineChateauGrapery','chateau_id','id');
    }
    //生产商
    public function produce()
    {
        return $this->belongsTo('App\Model\SysProduce','produce_id','id');
    }

    public function beforeSave($input=null)
    {
        if($this->online != $this->getOriginal('online')) {
            if ($this->online == "yes") {
                $this->online_at = Helpers::now();
            }
            if ($this->online == "no") {
                $this->offline_at = Helpers::now();
            }
        }

        return true;
    }
}
