<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Validation\Rule;

class UserCouponRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\UserCoupon";

    protected $allowApiMethod = ['index','show','store'];

    protected function commonRules()
    {
        return [
            'get_user_id' => [
              'required',
              Rule::exists('t_user','id')->whereNull('deleted_at'),
            ],
            'coupon_id' => [
                'required',
                Rule::exists('wine_coupons','id')->whereNull('deleted_at'),
            ],
        ];
    }

    protected function commonMessages()
    {
        return [
            'get_user_id.exists' => '用户不存在',
            'coupon_id.exists' => '优惠券活动不存在',
        ];
    }

    protected function commonSave($model)
    {
        $model->updateExpiredAt();
    }
}