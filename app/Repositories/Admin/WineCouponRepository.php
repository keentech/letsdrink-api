<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use App\Helpers\Helpers;
use App\Model\WineCouponCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class WineCouponRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineCoupon";

    protected $allowApiMethod = ['index','show','store','update','simpleList'];

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    protected function simpleListQuery()
    {
        return DB::table($this->query()->getTable())->whereNull('deleted_at');
    }

    protected function simpleListFiled()
    {
        return ['id','name','coupon_name'];
    }

    protected function storeRules()
    {
        return [
            'name' => 'required',
            'coupon_category_id' => [
                'required',
                Rule::exists('wine_coupon_categories','id')->whereNull('deleted_at')
            ],
            'can_get_start' => 'required|date',
            'can_get_end' => 'required|date',
            'enable_use_date' => 'required|in:no,yes',
            'enable_use_days' => 'required|in:no,yes',
            'can_use_start' => 'required_if:enable_use_date,yes',
            'can_use_end' => 'required_if:enable_use_date,yes',
            'can_use_days' => 'required_if:enable_use_days,yes',
            'condition' => 'required|in:all,old,new',
        ];
    }

    protected function commonRules()
    {
        return [
            'sku'=> 'required|numeric'
        ];
    }

    //新建更新公共验证部分
    protected function processStoreValidate()
    {

        $date = $this->request->input('enable_use_date');
        $days = $this->request->input('enable_use_days');

        if ($date== "yes" && $days == "yes"){
            $this->error = '使用有效期条件只能选择一个';
            return false;
        }

        return true;
    }

    protected function appendStoreFillData()
    {
        $wcc = WineCouponCategory::find($this->request->input('coupon_category_id'));

        if ($wcc) {
            return [
                'coupon_name'=>$wcc->couponName,
                'code' => Helpers::createCouponCode()
            ];
        }
        return [];
    }

    protected function getUpdateFillData()
    {
        return $this->request->only('sku');
    }
}