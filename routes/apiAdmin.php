<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API AdminRepository Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::any('me', 'AuthController@me');
    Route::post('change-password','AuthController@changePassword');         //修改密码
});

Route::resource('restful','ApiController');                             //通用resetFul接口
Route::post('destroyBatch','ApiController@destroyBatch');               //批量删除
Route::get('simplelist/{id?}','ApiController@simpleList');                    //列表
Route::post('file-upload','ResourceController@upload');                 //文件上传接口
Route::post('state-change','StateController@change');                   //状态切换接口
Route::get('export','ApiController@export');                            //导出
Route::get('qrcode-{id}-export','QRCodeController@export');             //导出二维码
Route::post('import-{action}','ImportController@action');               //导入
Route::get('product-category','ProductController@category');            //类别
Route::get('captcha','BaseController@captcha');                         //验证码

Route::post('express-carriers-update','ExpressController@updateCarrierList');       //更新物流运输商
Route::get('express-carriers-export','ExpressController@carriersExport');           //物流运输商导出

Route::get('order-export','OrderController@export');                                //导出
Route::post('order-delivery','OrderController@delivery');                           //发货
Route::post('order-delivery-batch','ExpressController@import');                     //批量发货
Route::get('order-delivery-batch-template','ExpressController@exportTemplate');     //订单物流单号批量导入模板
Route::get('order-express/{id}','OrderController@queryDelivery');                   //查询物流

Route::get('chart-visit','ChartController@visitChart');                   //浏览数据
Route::get('chart-order','ChartController@orderChart');                   //订单数据
Route::get('chart-new','ChartController@newChart');                      //新增数据

Route::get('areas','AreaController@index');                      //新增数据
Route::get('scene/tree','ProductController@sceneTree');                      //新增数据
Route::post('activity-order/{id}/changeState','ActivityController@changeState');                //通过活动订单状态
Route::post('activity-order/delivery','ActivityController@delivery');                      //活动订单发货
Route::get('activity/{id}/next/list','ActivityController@getNextSimpleList');               //活动列表
Route::post('activity/{id}/{nextId}','ActivityController@setNext');                      //活动订单发货