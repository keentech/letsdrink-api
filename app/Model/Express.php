<?php

namespace App\Model;

use App\BaseModel;
use App\Helpers\Trackingmore;

class Express extends BaseModel
{
    protected $table = "order_expresses";

    protected $appends = ['expressInfo','ChStatus'];

    protected $hidden = ['content'];

    public function carrier()
    {
        return $this->belongsTo('App\Model\ExpressCarrier','carrier_code','code');
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Order','out_trade_no','out_trade_no');
    }

    //创建单个运单号
    public function createTracking()
    {
        $track = new Trackingmore();
        $extraInfo['title']          = $this->title;
        $extraInfo['customer_name']  = $this->customer_name;
        $extraInfo['order_id']       = $this->out_trade_no;
        $extraInfo['lang']       = 'cn';
        $res = $track->createTracking($this->carrier_code,$this->tracking_number,$extraInfo);

        if ($res && $res['meta']['code']==200) {
            $this->tracking_id = $res['data']['id'];
            $this->state = $res['data']['state'];
            $this->save();
        }
        return $res;
    }

    //获取单个运单号实时物流信息
    public function getTrackingRealTime()
    {
        $track = new Trackingmore;
        $extraInfo['destination_code']          = 'CN';
        //$extraInfo['tracking_ship_date']  = '20180226';
        //$extraInfo['tracking_postal_code'] = '13ES20';
        $extraInfo['specialNumberDestination']       = 'CN';
        $extraInfo['order']       = $this->out_trade_no;
        $extraInfo['lang']       = 'cn';
        $res = $track->getRealtimeTrackingResults($this->carrier_code,$this->tracking_number,$extraInfo);

        if ($res && $res['meta']['code']==200) {
            $data = $res['data']['items'][0];

            $this->tracking_id = $data['id'];
            $this->status = $data['status'];
            $this->content = json_encode($data);
            $this->last_query_time = time();
            $this->save();
            return true;
        }
        return false;
    }
    public function getChStatusAttribute()
    {
        //pending	查询中：新增包裹正在查询中，请等待！
        //notfound	查询不到：包裹目前查询不到。
        //transit	运输途中：包裹正从发件国运往目的国。
        //pickup	到达待取：包裹正在派送中或已到达当地收发点，你可以继续派送或收件。
        //delivered	成功签收：包裹已成功妥投。
        //undelivered	投递失败：快递员尝试过投递但失败，（这种情况下）通常会留有通知并且会再次试投！
        //exception	可能异常：包裹出现异常，发生这种情况可能是包裹已退回寄件人，清关失败，包裹已丢失或损坏等。
        //expired	运输过久：包裹很长一段时间显示在运输途中，一直没有派送结果。

        $a = ['pending'=>'查询中','notfound'=>'查询不到','transit'=>'运输途中','pickup'=>'到达待取','delivered'=>'成功签收',
            'undelivered'=>'投递失败','exception'=>'可能异常','expired'=>'运输过久'];

        return isset($a[$this->status])?$a[$this->status]:$this->status;
    }
    public function getExpressInfoAttribute()
    {
        return $this->content ? json_decode($this->content):null;
    }

    //$second 相差多少可以更新信息
    public function updateTrackingRealTime($second=180)
    {
        if (!$this->last_query_time || ($this->last_query_time && $this->last_query_time + $second < time())) {
            return $this->getTrackingRealTime();
        }
        return false;
    }
}
