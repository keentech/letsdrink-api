<?php

namespace App\Model;

use App\BaseModel;

//产品档次
class WineGrading extends BaseModel
{
    protected $table = "wine_gradings";

    protected $fillable = [
        'name',
        'icon'
    ];

    protected $visible = ["id",'name',"icon"];


}
