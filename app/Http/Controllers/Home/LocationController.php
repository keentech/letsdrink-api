<?php

namespace App\Http\Controllers\Home;

use App\Model\UserLocation;
use App\Model\WeChatFromId;
use Illuminate\Http\Request;

class LocationController extends BaseController
{
    public function store(Request $request)
    {
        $data = $request->all();
        $m = new UserLocation();
        $m->fill($data);
        $m->user_id = $this->loginUser()?$this->loginUser()->id:0;
        $m->save();

        if ($formid = $request->input('formid')){
            WeChatFromId::createFromForm($formid,$this->loginUser()->weixinId);
        }

        return $this->success($m);
    }

    public function show($id)
    {
        $item = UserLocation::find($id);
        return $this->success($item);
    }
}
