<?php

namespace App\Http\Controllers\Home;

use App\Model\WineChateau;

class ChateauController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = WineChateau::with('chateauPic')
            ->orderBy('weight','desc')
            ->orderBy('updated_at','desc')
            ->paginate(6);

        return $this->success($items);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = WineChateau::with('chateauPic','chateauMasterPic','chateauGraperyPic','chateauGrapery','produce')
            ->find($id);

        return $this->success($item);
    }
}
