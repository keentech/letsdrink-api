<?php

namespace App\Model;

use App\BaseModel;

class WineProductRelation extends BaseModel
{
    protected $table = "wine_product_relation";
}
