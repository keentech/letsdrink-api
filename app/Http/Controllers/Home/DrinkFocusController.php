<?php

namespace App\Http\Controllers\Home;

use App\Model\Activity;
use App\Model\WineProduct;
use Illuminate\Support\Facades\DB;

class DrinkFocusController extends BaseController
{
    public function index()
    {
        $items = DB::table('wine_focus_picture')->where('weight','>',60)->whereNull('deleted_at')
            ->orderBy('weight','desc')
            ->select('id','title','pic','obj_id','type')
            ->get();

        $rows = [];
        foreach ($items as $item){
            if ($item->type == "article"){
                $row = DB::table('cms_article')->leftJoin('cms_article_column','cms_article_column.id','=','cms_article.column_id')
                    ->where('cms_article.id','=',$item->obj_id)->where('cms_article.state','=','publish')
                    ->where('cms_article.online','=','yes')->whereNull('cms_article.deleted_at')
                    ->select('cms_article.id','cms_article.title','cms_article_column.name')
                    ->first();
                if ($row) {
                    $item->object = $row;
                    $rows[] = $item;
                }
            }

            if ($item->type == "activity"){
                $row = Activity::where('id','=',$item->obj_id)->select('id','name','covers',
                    'detail_text','detail_pic','location', 'cost_type','cost','sign_count','start_time','end_time',
                    'deadline','toll','type')
                    ->first();
                if ($row) {
                    $row->products = DB::table('wine_product')
                        ->whereRaw('`id` in (select product_id from activity_products where activity_id=?)',[$row->id])
                        ->select('id','chname','name')->get();

                    $row->lecturer = DB::table('t_user')
                        ->where('id','=',[$row->lecturer_id])
                        ->select('id','name','intro','authTag','chAuthTag')->first();
                    $item->object = $row;
                    $rows[] = $item;
                }
            }

            if ($item->type == "product"){
                $m = WineProduct::with('covers')->where('online','=','yes')
                    ->find($item->obj_id);
                if ( $m ) {
                    //基础信息
                    $it['id'] = $m->id;
                    $it['name'] = $m->name;
                    $it['vintage'] = $m->vintage;
                    $it['covers'] = $m->covers;
                    $it['chname'] = $m->chname;
                    $it['price'] = $m->promotion_price;
                    $it['promotion_price'] = $m->promotion_price;
                    $it['show_cuisine'] = $m->show_cuisine;
                    $it['show_scenes'] = $m->show_scenes;
                    $it['letDrink_recom'] = $m->letDrink_recom;
                    $it['chateau'] =  null;

                    //酒庄信息
                    if ($m->chateau) {
                        $it['chateau']['id'] = $m->chateau->id;
                        $it['chateau']['name'] = $m->chateau->name;
                        $it['chateau']['chname'] = $m->chateau->chname;
                    }
                    $it['talk_keywords'] = null;
                    $it['tasteKeywords'] = [];

                    //味道关键词
                    if ($m->drink){
                        $it['talk_keywords'] = $m->drink->talk_keywords;
                        $it['tasteKeywords'] = $m->drink->tasteKeywords;
                    }

                    //产品档次
                    $it['wine_grading'] = null;
                    if ($m->wineGrading){
                        $it['wine_grading']['id'] = $m->wineGrading->id;
                        $it['wine_grading']['name'] = $m->wineGrading->name;
                        $it['wine_grading']['icon'] = $m->wineGrading->icon;
                    }

                    //消费场景
                    $it['scenes'] = $m->getScenesItemsAttribute();

                    //菜系
                    $it['cuisines'] = $m->cuisines;

                    //味道
                    $it['wine_tastes'] = $m->wineTastes;


                    $item->object = $it;
                    $rows[] = $item;
                }
            }
        }
        return $this->success($items);
    }
}
