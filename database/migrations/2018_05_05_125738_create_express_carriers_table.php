<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpressCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('express_carriers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('英文名称');
            $table->string('name_cn')->comment('中文名称');
            $table->string('code')->comment('快递简码');
            $table->string('phone')->comment('联系电话');
            $table->string('homepage')->comment('主页地址');
            $table->string('type')->comment('类型');
            $table->string('picture')->comment('封面图片');
            $table->integer('sort')->comment('排序');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('express_carriers');
    }
}
