<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Validation\Rule;

class WineFocusPictureRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineFocusPicture";

    protected function commonRules()
    {
        return [
            'title' => 'required|max:20',
            'pic' => 'required',
            'type' => [
                'required',
                Rule::in(['article','product','activity']),
            ],
            'obj_id'=>'required'
        ];
    }

    protected function relationLoad()
    {
        return ['object'];
    }

    protected function getShowRespondData($model)
    {
       return $model->load('object');
    }
}