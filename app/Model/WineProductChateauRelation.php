<?php

namespace App\Model;

use App\BaseModel;

class WineProductChateauRelation extends BaseModel
{
    protected $table = "wine_product_chateau_relation";

    protected $fillable =[
        'product_id',
        'obj_id',
        'user_id',
    ];
}
