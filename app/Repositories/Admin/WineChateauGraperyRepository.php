<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class WineChateauGraperyRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineChateauGrapery";

    protected function getShowRespondData($model)
    {
        return $model->load('chateau','user');
    }

    //更新的验证规则
    protected function updateRules($id)
    {
        return [
            'chateau_id' => 'required',
            'name' => 'required',
            'proportion' => 'required|min:0|max:100',
        ];
    }

    //保存的验证规则
    protected function storeRules()
    {
        return [
            'chateau_id' => 'required',
            'name' => 'required',
            'proportion' => 'required|min:0|max:100',
        ];
    }

    //获取store保存的数据
    protected function getStoreFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        return array_merge($data,['user_id'=>$this->loginUser->id]);
    }

    //获取update保存的数据
    protected function getUpdateFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        return array_merge($data,['user_id'=>$this->loginUser->id]);
    }
}