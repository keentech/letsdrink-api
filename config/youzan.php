<?php
return [
    'client_id' => env('YOUZAN_CLIENT_ID'),
    'client_secret' => env('YOUZAN_CLIENT_SECRET'),
    'type' => \Hanson\Youzan\Youzan::PERSONAL, // 自用型应用
    'debug' => true, // 调试模式
    'kdt_id' => env('YOUZAN_KDT_ID'), // 店铺ID
    'log' => [
        'name' => 'youzan',
        'file' => storage_path('logs/youzan.log'),
        'level'      => 'debug',
        'permission' => 0777,
    ],
];
?>