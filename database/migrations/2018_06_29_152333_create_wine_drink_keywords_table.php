<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWineDrinkKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_drink_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->comment('酒id');
            $table->integer('drink_id')->comment('drink id');
            $table->integer('keyword_id')->comment('');
            $table->string('keyword')->comment('关键词');
            $table->string('keyword_pic')->comment('关键词图标');
            $table->string('text');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_drink_keywords');
    }
}
