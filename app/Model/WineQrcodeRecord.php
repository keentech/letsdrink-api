<?php

namespace App\Model;

use App\BaseModel;

class WineQrcodeRecord extends BaseModel
{
    protected $table = "wine_qrcode_record";

    //关联产品
    public function product()
    {
        return $this->belongsTo('App\Model\WineProduct','product_id','id');
    }
    //关联发放的二维码记录
    public function productQrcode()
    {
        return $this->belongsTo('App\Model\WineProductQrcode','qrcode_id','id');
    }

    public static function findByCode($code)
    {
        return self::where('code','=',$code)->first();
    }

    public function changeState($state)
    {
        $this->state = $state;
        $this->save();
    }

    public static function updateState($code,$state=1)
    {
        if($m = WineQrcodeRecord::findByCode($code)){
            $m->state = $state;
            $m->save();
            if ($pq = $m->productQrcode){
                $pq->updateRemainAmount();
            }
        }
    }
}
