<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_dealers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment("经销商名称");
            $table->string('tel')->comment("联系电话");
            $table->string('remark')->comment("备注");
            $table->string('area_code')->comment("地址代码");
            $table->string('area_name')->comment("地址");
            $table->integer('user_id');
            $table->string('updater');
            $table->string('creator');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_dealers');
    }
}
