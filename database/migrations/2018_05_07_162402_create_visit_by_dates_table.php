<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitByDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_visit_by_date', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->comment('日期');
            $table->integer('obj_type')->default(0)->comment('访问类型:1文章|2葡萄酒');
            $table->integer('total')->default(0)->comment('葡萄酒详情页总浏览量');
            $table->integer('qr_pv')->default(0)->comment('二维码来源浏览量');
            $table->integer('qr_uv')->default(0)->comment('二维码来源浏览人数');
            $table->integer('pv')->default(0)->comment('普通来源浏览量');
            $table->integer('uv')->default(0)->comment('普通来源浏览人数');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * 
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_visit_by_date');
    }
}
