<?php

namespace App\Http\Controllers\Home;

use App\Model\Order;

class GiftController extends BaseController
{
    public function index()
    {
        $user_id = $this->loginUserId();

        $notGetItems = Order::with('getUser','createUser','product','product.covers',
            'product.userHasDrink','product.userWantDrink',"product.wineGrading","product.scenes",'product.drinkTasteKeywords',
            'product.cuisines','product.wineTastes')
            ->where('create_user_id','=',$user_id)
            ->where('type','=','gift')
            ->whereNull('deleted_at')
            ->whereRaw('(get_user_id is null or get_user_id<1)')
            ->where('state','=',3)
            ->orderBy('created_at','desc')
            ->get();

        $hasGetItems = Order::with('getUser','createUser','product','product.covers',
            'product.userHasDrink','product.userWantDrink',"product.wineGrading","product.scenes",'product.drinkTasteKeywords',
            'product.cuisines','product.wineTastes')
            ->where('create_user_id','=',$user_id)
            ->where('type','=','gift')
            ->whereNull('deleted_at')
            ->where('get_user_id','>',0)
            ->orderBy('created_at','desc')
            ->get();

        return $this->success(['not_get_items'=>$notGetItems,'has_get_items'=>$hasGetItems]);
    }

    public function index2()
    {
        $user_id = $this->loginUserId();

        $getItems = Order::with('getUser','createUser','product','product.covers',
            'product.userHasDrink','product.userWantDrink',"product.wineGrading","product.scenes",'product.drinkTasteKeywords',
            'product.cuisines','product.wineTastes')
            ->where('get_user_id','=',$user_id)
            ->whereNull('deleted_at')
            ->where('type','=','gift')
            ->orderBy('created_at','desc')
            ->get();

        $sendItems = Order::with('getUser','createUser','product','product.covers',
            'product.userHasDrink','product.userWantDrink',"product.wineGrading","product.scenes",'product.drinkTasteKeywords',
            'product.cuisines','product.wineTastes')
            ->where('create_user_id','=',$user_id)
            ->where('type','=','gift')
            ->whereNull('deleted_at')
            ->whereNotIn('state',[0,7])
            ->orderBy('created_at','desc')
            ->get();

        return $this->success(['get_items'=>$getItems,'send_items'=>$sendItems]);
    }
}
