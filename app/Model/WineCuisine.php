<?php

namespace App\Model;

use App\BaseModel;

//菜系选择
class WineCuisine extends BaseModel
{
    protected $table = "wine_cuisines";

    protected $fillable = [
        'name',
        'icon'
    ];

    protected $visible = ["id",'name',"icon"];

}
