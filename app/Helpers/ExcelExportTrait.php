<?php

namespace App\Helpers;

use Maatwebsite\Excel\Facades\Excel;

trait ExcelExportTrait
{
    public function exportFile($outputName,$data,$ext="xlsx",$style=null)
    {
        return Excel::create($outputName,function ($excel) use($data,$style){
            $excel->sheet('sheet', function($sheet) use($data,$style) {
                $sheet->setAutoSize(true);
                if (isset($style['width']) && is_array($style['width'])){
                    $sheet->setWidth($style['width']);
                }
                $sheet->fromArray($data);
            });
        })->download($ext);
    }
}
