<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionIdToWineProductQrcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wine_product_qrcode', function (Blueprint $table) {
            $table->integer('region_id')->comment('产区id');
            $table->integer('subdivision_id')->comment('地区id');
            $table->integer('country_id')->comment('国家id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wine_product_qrcode', function (Blueprint $table) {
            //
        });
    }
}
