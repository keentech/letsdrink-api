<?php

namespace App\Model;

use App\BaseModel;

class WineProductJudge extends BaseModel
{
    protected $table = "wine_product_judge";

    protected $fillable = [ 'product_id','letDrink_judge','user_id'];

    protected $appends = ['expert_judges'];

    protected $hidden = ['judges'];

    public function judges()
    {
        return $this->hasMany('App\Model\WineProductExpertJudge','judge_id','id')
            ->orderBy('created_at','desc');
    }

    public function getExpertJudgesAttribute()
    {
        return $this->judges;
    }
}
