<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('toll')->default(1)->comment('是否收费:1收费|0免费');
            $table->string('type')->comment('活动类型:article文章|poster海报');
            $table->string('state')->default('draft')->comment('状态:publish|draft|finished');
            $table->string('name')->comment('活动名称');
            $table->string('abstract')->nullable()->comment('摘要');
            $table->text('covers')->nullable()->comment('封面图片');
            $table->text('detail_text')->nullable()->comment('活动详文字');
            $table->text('detail_pic')->nullable()->comment('活动详情图片');

            $table->string('lecturer_id')->nullable()->comment('活动讲师');
            $table->integer('participate_limit')->comment('参与人员限制');
            $table->integer('need_share')->comment('是否需要用户分享:0不需要|1需要');
            $table->integer('share_rule')->nullable()->comment('用户分享规则');
            $table->dateTime('start_time')->comment('活动时间开始');
            $table->dateTime('end_time')->comment('活动时间结束');
            $table->date('deadline')->comment('报名截止');
            $table->string('location')->nullable()->comment('活动地点');
            $table->integer('cost_type')->nullable()->comment('活动费用类型:0免费|2两人|3人');
            $table->float('cost',12,2)->nullable()->comment('活动费用');
            $table->integer('user_id')->comment('创建人id');
            $table->string('creator')->comment('创建人');
            $table->string('updater')->comment('更新人');

            $table->integer('click_count')->comment('点击量');
            $table->integer('sign_count')->comment('已报名人数');
            $table->integer('collect_count')->comment('收藏数');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
