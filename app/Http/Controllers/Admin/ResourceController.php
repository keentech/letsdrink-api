<?php

namespace App\Http\Controllers\Admin;

use App\Model\Resource;
use Illuminate\Http\Request;

class ResourceController extends BaseController
{
    //文件上传
    public function upload(Request $request)
    {
        if($request->hasFile('file')) {

            $file = $request->file('file');

            if(!$file->isValid()) {
                return $this->failed('文件无效');
            }

            $uploadType = $request->input('uploadType','qiniu');

            if ( $resource = Resource::saveUploadFile($file,$uploadType)) {
                return $this->success($resource);
            }
        }
        if($request->hasFile('upload')) {

            $file = $request->file('upload');

            if(!$file->isValid()) {
                return response()->json(array('uploaded'=>false,'status'=>'error','message'=>'文件无效','code' => 200,'url'=>null,'data'=>null));
            }

            $uploadType = $request->input('uploadType','qiniu');

            if ( $resource = Resource::saveUploadFile($file,$uploadType)) {
                return response()->json(array('uploaded'=>true,'status'=>'success','code' => 200,'url' => $resource->imgSrv,'data'=>$resource));
            }
        }
        if($request->hasFile('imgFile')) {
            
            $file = $request->file('imgFile');

            if(!$file->isValid()) {
                return $this->failed('文件无效');
            }

            $uploadType = $request->input('uploadType','qiniu');

            if ( $resource = Resource::saveUploadFile($file,$uploadType)) {
                return response()->json(array('error' => 0, 'url' => $resource->imgSrv));
            }
        }

        return $this->failed('上传失败');
    }
}
