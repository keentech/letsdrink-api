<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class WineTodayPotablesRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\TodayPotable";

    protected function commonRules()
    {
        return [
            'title' => 'required',
            'online_at'=>'required|date',
        ];
    }
}