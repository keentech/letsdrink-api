<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->comment('用户id');
            $table->string('origin')->comment('来源');
            $table->string('latitude')->comment('纬度，浮点数，范围为-90~90，负数表示南纬');
            $table->string('longitude')->comment('经度，浮点数，范围为-180~180，负数表示西经');
            $table->string('speed')->comment('速度，浮点数，单位m/s');
            $table->string('accuracy')->comment('位置的精确度');
            $table->string('altitude')->comment('高度，单位 m');
            $table->string('verticalAccuracy')->comment('垂直精度，单位 m（Android 无法获取，返回 0）');
            $table->string('horizontalAccuracy')->comment('水平精度，单位 m');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_locations');
    }
}
