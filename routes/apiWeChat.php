<?php


/*
|--------------------------------------------------------------------------
| API AdminRepository Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('me',    'WeChatController@me');//我的信息（会保存在数据库）
Route::any('share', 'WeChatController@share');//分享配置
Route::any('user',  'WeChatController@userInfo');//我的信息（不保存数据库）
Route::any('login',  'WeChatController@wechatLogin');//微信登陆
Route::any('payNotify',  'WeChatController@paymentNotify');//微信支付结果通知
Route::any('refundNotify',  'WeChatController@refundNotify');//微信退款结果通知

Route::group(['middleware'=>'memJWT'],function (){
    Route::post('session','WeChatController@getSession');//获取session
    Route::post('mobile','WeChatController@getMobile');//获取手机号
    Route::post('prepay','WeChatController@prepay');//支付sdk
});