<?php

namespace App\Model;

use App\BaseModel;

class UserAddress extends BaseModel
{
    protected $table = "t_user_addresses";

    protected $fillable = [
        'user_id',
        'default',
        'userName',
        'postalCode',
        'provinceName',
        'cityName',
        'countyName',
        'detailInfo',
        'nationalCode',
        'telNumber',
    ];

    public function getCompleteAddressAttribute()
    {
        return $this->provinceName.' '.$this->cityName.' '.$this->countyName.' '.$this->detailInfo;
    }
}
