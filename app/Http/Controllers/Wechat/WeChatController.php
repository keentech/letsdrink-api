<?php

namespace App\Http\Controllers\Wechat;

use App\Helpers\ApiResponseTrait;
use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderPayRecord;
use App\Model\WeChatFromId;
use App\Model\WineProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use \EasyWeChat\MiniProgram\Application;

class WeChatController extends Controller
{
    use ApiResponseTrait;

    public function getSession(Application $app,Request $request)
    {
        $code = $request->input('code');
        if (!$code){
            return $this->failed('not have code');
        }

        $session = $app->auth->session($code);

        return $this->success($session);
    }

    public function getMobile(Application $app,Request $request)
    {
        $user = $this->loginUser();

        Log::info(json_encode($request->all()));

        $rules = [
            //'session' => 'required',
            'iv' => 'required',
            'encryptedData' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()){
            return $this->failed($validator->errors()->first());

        }
//        $session = $request->input('session');
//        $session = !is_string($session)?json_encode($session):$session;
//        $session = json_decode($session,true);

//        $session_json = Helpers::getTempJson('wechat_session');
//        $session = json_decode($session_json,true);

        $session = [
            'openid' => $user->weixinId,
            'session_key' => $user->session_key
        ];

        $iv = $request->input('iv');
        $encryptedData = $request->input('encryptedData');

        Log::info('session_key'.$session['session_key'] .'iv'. $iv .'data:'. $encryptedData);

        $resp = $app->encryptor->decryptData($session['session_key'], $iv,$encryptedData);

        if ($resp && isset($resp['phoneNumber']) && $resp['phoneNumber'] &&  $user){
            $user->mobile = $resp['countryCode']==86?$resp['phoneNumber']:('+'.$resp['countryCode'].$resp['phoneNumber']);
            $user->save();
        }
        return $this->success($resp);
    }

    public function prepay(\EasyWeChat\Payment\Application  $app,Request $request)
    {
        $user = $this->loginUser();
        if (!$user) return $this->failed('请先登录');
        $id = $request->input('order_id');

        $order = Order::where('create_user_id','=',$user->id)
            ->where('state','=',0)
            ->where('is_pay','=','no')->find($id);

        if (!$order) return $this->failed('订单不存在');

        $product = WineProduct::find($order->product_id);

        if (!$product) {
            return $this->failed('商品不存在或已下架');
        }

        if ($product->sell_method != "online") {
            return $this->failed('商品已下架');
        }

        if ($product->sku < $order->quantity) {
            return $this->failed('商品库存不足');
        }

        $openid = $user->weixinId;

        $total_fee = $order->total_fee;
        $attr = [
            'body' => $order->product_name,
            'out_trade_no' => $order->out_trade_no,
            'total_fee' => $total_fee*100,
            'notify_url' => env('WECHAT_PAYMENT_NOTIFY_URL'), // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            'trade_type' => 'JSAPI',
            'openid' => $openid,
        ];



        $result = $app->order->unify($attr);
        $this->log($result);

        if ($result && $result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS'){

            $record = OrderPayRecord::where('order_id','=',$id)
                ->where('openid','=',$attr['openid'])
                ->where('trade_state','=','NOTPAY')
                ->first();

            //保存支付记录（当前未支付）
            if (!$record) {
                $record = new OrderPayRecord();
                $record->order_id = $order->id;
                $record->openid = $attr['openid'];
                $record->out_trade_no = $attr['out_trade_no'];
                $record->trade_type = $attr['trade_type'];
                $record->trade_state = 'NOTPAY';
                $record->total_fee = $total_fee;
                $record->notify_url = $attr['notify_url'];
                $record->save();
            }

            $prepayId = $result['prepay_id'];
            $configForJSSDK = $app->jssdk->sdkConfig($prepayId);

            //记录form_id用于发送模板消息3次
            WeChatFromId::createFromPrepay($prepayId,$openid);

            if ($formid = $request->input('formid')){
                WeChatFromId::createFromForm($formid,$this->loginUser()->weixinId);
            }

            $this->log('prepayConfig:'.json_encode($configForJSSDK));

            return $this->success($configForJSSDK);
        }

        return $this->failed('fail');
    }

    //支付回掉
    public function paymentNotify(\EasyWeChat\Payment\Application  $app,Request $request)
    {
        $this->log('wechat_pay_notify');

        $response = $app->handlePaidNotify(function ($message,$fail){
            Helpers::log('paymentNotify '.json_encode($message),'payment');
            try{
                if ($message['return_code'] === 'SUCCESS') {
                    OrderPayRecord::paymentNotify($message);
                } else {
                    return $fail('通信失败，请稍后再通知我');
                }
            }catch (\Exception $e){
                Log::error($e->getMessage());
                return $fail('通信失败，请稍后再通知我');
            }
            return true;
        });
        return $response;
    }

    //退款回掉
    public function refundNotify(\EasyWeChat\Payment\Application  $app)
    {
        $this->log('wechat_refund_notify');

        $response = $app->handleRefundedNotify(function ($message, $reqInfo, $fail) {
            Helpers::log('[refundNotify] '.json_encode($reqInfo),'payment');
            try{
                OrderPayRecord::refundNotify($message, $reqInfo);
                return true;
            }catch (\Exception $e){
                Log::error($e->getMessage());
                return $fail('通信失败，请稍后再通知我');
            }
            return true;
        });

        return $response;
    }
    private function log($msg)
    {
        Helpers::log($msg,'payment');
    }
}
