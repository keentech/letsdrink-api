<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPosterToCmsArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_article', function (Blueprint $table) {
            $table->string('poster')->nullable()->comment('海报');
            $table->dateTime('poster_create')->nullable()->comment('海报生成时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_article', function (Blueprint $table) {
            //
        });
    }
}
