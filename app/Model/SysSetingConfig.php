<?php

namespace App\Model;

use App\BaseModel;

class SysSetingConfig extends BaseModel
{
    protected $table = "sys_seting_configs";

    protected $fillable = ['key','value','user_id','creator','updater','show_frontend'];
}
