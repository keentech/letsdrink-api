<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class SysAreasRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\Area";
}