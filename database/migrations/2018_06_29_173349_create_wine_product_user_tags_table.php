<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWineProductUserTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_product_user_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('auth')->comment('身份');
            $table->string('sex')->comment('性别');
            $table->string('age')->comment('年龄');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_product_user_tags');
    }
}
