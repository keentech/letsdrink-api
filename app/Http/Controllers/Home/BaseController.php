<?php

namespace App\Http\Controllers\Home;

use App\Helpers\ApiResponseTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BaseController extends Controller
{
    use ApiResponseTrait;

    protected $error;

    protected function requestValidate($data,$rules=[],$message=[])
    {
        $validator = Validator::make($data,$rules,$message);
        if ($validator->fails()){
            $this->error = $validator->errors()->first();
            return false;
        }
        return true;
    }

    public function frontendSeting()
    {
        $items = DB::table('sys_seting_configs')->where('show_frontend','=',1)->select('key','value')->get();

        return $this->success($items);
    }
}
