<?php

namespace App\Model;

use App\BaseModel;

class CaptchaRecord extends BaseModel
{
    protected $table = "sys_captcha_records";

    public static function findByKey($key)
    {
        return self::where('key','=',$key)->first();
    }
}
