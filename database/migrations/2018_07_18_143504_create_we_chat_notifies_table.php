<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeChatNotifiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_wechat_notifies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->comment('消息标题');
            $table->integer('user_id')->comment('用户id');
            $table->string('openid')->comment('用户id');
            $table->string('username')->nullable()->comment('用户名');
            $table->text('content')->nullable()->comment('发送内容');
            $table->string('state')->nullable()->comment('发送状态');
            $table->string('state_msg')->nullable()->comment('发送状态描述');
            $table->integer('send_count')->comment('发送次数');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_wechat_notifies');
    }
}
