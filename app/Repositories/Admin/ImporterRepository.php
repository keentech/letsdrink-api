<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use App\Model\Area;

class ImporterRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\Importer";

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    protected function commonRules()
    {
        return [
            'name' => 'required',
            'tel' => 'required',
        ];
    }

    protected function appendCommonFillData()
    {
        if ($code = $this->request->input('area_code')){
            if( $area = Area::findByCode($code)){
                return [
                    'area_code' => $code,
                    'area_name' => $area->area_name
                ];
            }
        }
        return [];
    }

}