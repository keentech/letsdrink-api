<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            1=>['name' => '系统管理员','code' => 'super','state' => 1],
            2=>['name' => '管理员','code' => 'admin','state' => 1],
            3=>['name' => '会员','code' => 'vip','state' => 1],
            4=>['name' => '专家','code' => 'expert','state' => 1],
            5=>['name' => '产品录入员','code' => 'editor','state' => 1],
            6=>['name' => '达人','code' => 'talent','state' => 1],
        ];

        \Illuminate\Support\Facades\DB::select('truncate table t_role');

        foreach ($data as $k=>$v){
            $m = new \App\Model\Role();
            $m->id = $k;
            $m->fill($v);
            $m->save();
        }
    }
}
