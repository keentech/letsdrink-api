<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToSysDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_dealers', function (Blueprint $table) {
            $table->string('latitude')->comment('纬度，浮点数，范围为-90~90，负数表示南纬');
            $table->string('longitude')->comment('经度，浮点数，范围为-180~180，负数表示西经');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_dealers', function (Blueprint $table) {
            //
        });
    }
}
