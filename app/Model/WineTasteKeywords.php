<?php

namespace App\Model;

use App\BaseModel;

class WineTasteKeywords extends BaseModel
{
    protected $table = "wine_taste_keywords";

    protected $fillable = [
        'name',
        'icon'
    ];

    protected $visible = ["id",'name',"icon"];
}
