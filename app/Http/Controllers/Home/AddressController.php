<?php

namespace App\Http\Controllers\Home;

use App\Model\UserAddress;
use App\Model\WeChatFromId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = UserAddress::where('user_id','=',$this->loginUser()->id)
            ->orderBy('default','desc')
            ->orderBy('created_at','desc')
            ->get();

        return $this->success($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'userName' => 'required',
            'postalCode' => 'required',
            'provinceName' => 'required',
            'cityName' => 'required',
            'countyName' => 'required',
            'detailInfo' => 'required',
            //'nationalCode' => 'required',
            // 'telNumber' => 'required|regex:/^1[345678][0-9]{9}$/',
            'telNumber' => 'required'
        ];

        $msg = [
            'telNumber.regex' => '无效的手机号码'
        ];

        if (!$this->requestValidate($request->all(),$rules,$msg)){
            return $this->failed($this->error);
        }

        DB::table('t_user_addresses')->where('user_id','=',$this->loginUser()->id)
            ->update(['default'=>0]);

        $m = new UserAddress();
        $m->fill($request->all());
        $m->user_id = $this->loginUser()->id;
        $m->default = 1;
        $m->save();

        if ($formid = $request->input('formid')){
            WeChatFromId::createFromForm($formid,$this->loginUser()->weixinId);
        }
        return $this->success($m);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = UserAddress::find($id);
        return $this->success($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'userName' => 'required',
            'postalCode' => 'required',
            'provinceName' => 'required',
            'cityName' => 'required',
            'countyName' => 'required',
            'detailInfo' => 'required',
            //'nationalCode' => 'required',
            'telNumber' => 'required|regex:/^1[345678][0-9]{9}$/',
        ];

        $msg = [
            'telNumber.regex' => '无效的手机号码'
        ];

        if (!$this->requestValidate($request->all(),$rules,$msg)){
            return $this->failed($this->error);
        }

        if ($m = UserAddress::find($id)){
            $m->fill($request->all());
            $m->save();
            return $this->success($m);
        }

        return $this->notFond();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($m = UserAddress::find($id)){
            $m->delete();

            $item = UserAddress::where('user_id','=',$this->loginUser()->id)
                ->orderBy('default','desc')
                ->orderBy('created_at','desc')
                ->first();
            if ($item){
                $this->setDefault($item->id);
            }

            return $this->message('删除成功');
        }

        return $this->notFond();
    }

    public function setDefault($id)
    {
        if ($m = UserAddress::find($id)){
            DB::table('t_user_addresses')->where('user_id','=',$this->loginUser()->id)->update(['default'=>0]);
            $m->default = 1;
            $m->save();
            return $this->message('设置成功');
        }

        return $this->notFond();
    }
}
