<?php

namespace App\Http\Controllers\Home;

use App\Model\NewUserRecord;
use App\Model\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitController extends BaseController
{
    //访问记录
    public function store(Request $request)
    {
//        $obj_id = intval($request->input('id'));
//        $obj_type = intval($request->input('type'));
//        $origin =intval($request->input('origin'));
//
//        if (in_array($obj_type,[1,2]) && in_array($origin,[0,1])) {
//            Visit::createRecord($obj_id,$obj_type,$origin);
            return $this->message('ok');
//        }
//        return $this->failed('fail');

    }

    public function newUser(Request $request)
    {
        $user = $this->loginUser();
        $type = $request->input('type');
        $origin = $request->input('origin');


        $types = ['view'=>0,'order'=>1];
        $origins = ['other'=>0,'qrcode'=>1];
        if ($user && isset($types[$type]) && isset($origins[$origin])){
            $c = DB::table('sys_new_user_records')->where('user_id','=',$user->id)->whereNull('deleted_at')->count();
            if ($c == 0) {
                $m = new NewUserRecord();
                $m->user_id = $user->id;
                $m->type = $types[$type];
                $m->origin = $origins[$origin];
                $m->save();
                return $this->message('ok');
            }
        }

        return $this->failed('fail');

    }
}
