<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

trait VisitTrait
{
    public function visitType()
    {
        $a = ['cms_article'=>1,'wine_product'=>2];
        return $a[$this->table];
    }

    public function updateVisit()
    {
        $pv = DB::select('select count(id) as total from sys_visits where obj_id=:obj_id and obj_type=:obj_type',
            ['obj_id'=>$this->id,'obj_type'=>$this->visitType()]);

        $uv = DB::select('select count(user_id) as total from (select user_id from sys_visits where obj_id=:obj_id and obj_type=:obj_type and user_id<>0 group by user_id) con',
            ['obj_id'=>$this->id,'obj_type'=>$this->visitType()]);

        $this->pv = $pv[0]->total;
        $this->uv = $uv[0]->total;
        $this->save();
    }
}
