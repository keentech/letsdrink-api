<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Validation\Rule;

class SysProduceRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\SysProduce";

    protected $appendUserId = true;

    protected $appendCreatorAndUpdater = true;

    protected function commonRules()
    {
        return [
            'name' => 'required',
            'chname' => 'required',
            //'logo' => 'required',
        ];
    }

    protected function storeRules()
    {
        return [
            'produceCode' => [
                'required',
                Rule::unique('sys_produce')->whereNull('deleted_at')
            ],
        ];
    }

    protected function updateRules($id)
    {
        return [
            'produceCode' => [
                'required',
                Rule::unique('sys_produce')->ignore($id)->whereNull('deleted_at')
            ],
        ];
    }

}