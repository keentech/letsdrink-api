<?php

namespace App\Model;

use App\BaseModel;

class WeChatFromId extends BaseModel
{
    protected $table = "t_user_wechat_from_ids";

    public static function createFromPrepay($prepayId,$openid)
    {
        $m = new WeChatFromId();
        $m->form_id = $prepayId;
        $m->openid = $openid;
        $m->count = 3;
        $m->expire_at = date('Y-m-d H:i:s',time()+604800 - 60);
        return $m->save();
    }

    public static function createFromForm($fromId,$openid,$count=1)
    {
        if ($fromId&&$openid){
            $m = new WeChatFromId();
            $m->form_id = $fromId;
            $m->openid = $openid;
            $m->count = $count;
            $m->expire_at = date('Y-m-d H:i:s',time()+604800 -60);
            return $m->save();
        }
        return false;
    }

    public static function getOneFormId($openid)
    {
        return WeChatFromId::where('openid','=',$openid)
            ->where('count','>',0)
            ->where('expire_at','>', date('Y-m-d H:i:s'))
            ->orderBy('expire_at','asc')
            ->first();
    }
}
