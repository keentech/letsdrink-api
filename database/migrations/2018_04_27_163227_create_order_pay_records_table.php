<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPayRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pay_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->comment('订单id');
            $table->string('out_trade_no')->comment('商户订单号');
            $table->string('openid')->comment('用户标识openid');
            $table->string('trade_type')->comment('交易类型（详见微信支付文档）')->nullable();
            $table->string('trade_state')->comment('交易状态SUCCESS—支付成功|REFUND—转入退款|NOTPAY—未支付|CLOSED—已关闭|REVOKED—已撤销（刷卡支付）|USERPAYING--用户支付中|PAYERROR--支付失败(其他原因，如银行返回失败)');
            $table->string('bank_type')->comment('付款银行')->nullable();
            $table->decimal('total_fee',12,2)->comment('标价金额');
            $table->string('transaction_id')->comment('微信支付订单号')->nullable();
            $table->string('attach')->comment('附加数据')->nullable();
            $table->dateTime('time_end')->comment('支付完成时间')->nullable();
            $table->string('trade_state_desc')->comment('交易状态描述')->nullable();

            $table->string('refund_fee')->comment('退款金额')->nullable();
            $table->string('refund_desc')->comment('退款原因')->nullable();
            $table->string('notify_url')->comment('退款结果通知url')->nullable();

            $table->string('refund_id')->comment('微信退款单号')->nullable();
            $table->string('refund_result_code')->comment('退款申请接收状态SUCCESS/FAIL')->nullable();
            $table->string('refund_status')->comment('退款状态：SUCCESS—退款成功|REFUNDCLOSE—退款关闭|PROCESSING—退款处理中|CHANGE—退款异常')->nullable();;
            $table->string('refund_account')->comment('退款资金来源')->nullable();
            $table->string('refund_recv_accout')->comment('退款入账账户')->nullable();
            $table->dateTime('refund_success_time')->comment('退款成功时间')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pay_records');
    }
}
