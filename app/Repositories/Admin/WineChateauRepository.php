<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use App\Model\ResourceTable;
use App\Model\WineChateauGrapery;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class WineChateauRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineChateau";

    protected function commonRules()
    {
        return [
            //'state' => 'required|in:publish,draft',
            //'summary' =>'required',
            //'chateau_grapery' => 'max:20',

            'name' => 'required',
            'chname' => 'required',
            'chateau_pic' => 'required',
            'produce_id' => 'required',
            'address' => 'required',
            'intro' => 'required',
            'feature' => 'required',
            //'owner' => 'required',
            //'owner_intro' => 'required',
            //'chateau_master_pic' => 'required',
        ];
    }

    //更新的验证规则
    protected function updateRules($id)
    {
        return [
            'chateauCode' => [
                'required',
                'max:5',
                Rule::unique('wine_chateau','chateauCode')->ignore($id)->whereNull('deleted_at')
            ],
        ];
    }

    //保存的验证规则
    protected function storeRules()
    {
        return [
            'chateauCode' => [
                'required',
                'max:5',
                Rule::unique('wine_chateau','chateauCode')->whereNull('deleted_at')
            ],
        ];
    }

    protected function commonMessages()
    {
        return [
            'chateauCode.max'=>'酒庄编码不能超过5位',
            'chateau_grapery.max' => '最多添加20种葡萄',
        ];
    }

    //加载
    protected function getShowRespondData($model)
    {
        return $model->load('chateauPic','chateauMasterPic','chateauGraperyPic','chateauGrapery','produce');
    }

    protected function getUpdateRespondData($model)
    {
        return $model->load('chateauPic','chateauMasterPic','chateauGraperyPic','chateauGrapery','produce');
    }

    //保存图片

    /**
     * @param $model
     *
     * pic_ids 酒庄图片chateau
     * master_pic_ids 庄主照片chateau_master
     * grapery_pic_ids 葡萄园图片chateau_grapery
     */
    private function savePic($model)
    {
        function pic_save($pics,$model,$type,$user_id) {
            $rows =$pics?explode(',',$pics) : [];

            $db = DB::table('sys_resource_table')->where('obj_id','=',$model->id)
                ->where('obj_type','=',$type);

            $o_ids = $db->pluck('resource_id')->toArray();

            $db->whereIn('resource_id',array_diff($o_ids,$rows))->delete();

            $rows = array_diff($rows,$o_ids);

            foreach ($rows as $row) {
                $AR = new ResourceTable();
                $AR->obj_id = $model->id;
                $AR->resource_id = $row;
                $AR->user_id = $user_id;
                $AR->obj_type = $type;
                $AR->save();
            }
        }

        //酒庄图片chateau
        if($this->request->has('pic_ids')){
            pic_save($this->request->input('pic_ids'),$model,'chateau',$this->loginUser->id);
        }

        //庄主照片chateau_master
        if($this->request->has('master_pic_ids')){
            pic_save($this->request->input('master_pic_ids'),$model,'chateau_master',$this->loginUser->id);
        }

        //葡萄园图片chateau_grapery
        if($this->request->has('grapery_pic_ids')){
            pic_save($this->request->input('grapery_pic_ids'),$model,'chateau_grapery',$this->loginUser->id);
        }
    }

    //保存葡萄酒(数组)
    private function saveGrapery($model)
    {
        if($this->request->has('chateau_grapery')) {
            DB::table('wine_chateau_grapery')->where('chateau_id','=',$model->id)->delete();
            $rows = $this->request->input('chateau_grapery');
            foreach ($rows as $row) {
                if (isset($row['name']) && $row['name'])  {
                    $m = new WineChateauGrapery();
                    $m->name = $row['name'];
                    if (isset($row['proportion'])) {
                        $m->proportion = $row['proportion'];
                    }
                    $m->chateau_id = $model->id;
                    $m->user_id = $this->loginUser->id;
                    $m->save();
                }
            }
        }
    }

    protected function afterStore($model)
    {
        $this->savePic($model);
        $this->saveGrapery($model);
    }

    protected function afterUpdate($model)
    {
        $this->savePic($model);
        $this->saveGrapery($model);
    }

    //获取保存到数据库的数据
    protected function getStoreFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        $attr = [
            'user_id' => $this->loginUser->id,
            'creator' => $this->loginUser->name?$this->loginUser->name:'',
            'state' => 'draft',
        ];

        if ($this->loginUser->userType == "admin"){
            $attr['state'] = "publish";
        }
        if ($this->loginUser->userType == "editor"){
            $attr['state'] = "pending";
        }

        return array_merge($data,$attr);
    }

    //获取保存到数据库的数据
    protected function getUpdateFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        $d = [
            'updater' => $this->loginUser->name?$this->loginUser->name:'',
        ];
        return array_merge($data,$d);
    }
}