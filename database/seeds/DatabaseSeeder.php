<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);

        //$this->seedUser();

        //$this->seedArticle();
        //$this->call(AreasTableSeeder::class);

        $this->call(CuisinesTableSeeder::class);
        $this->call(GradingsTableSeeder::class);
        $this->call(SceneTableSeeder::class);
        $this->call(TastesTableSeeder::class);
        $this->call(TasteKeywordsTableSeeder::class);
        $this->call(UserAuthTagTableSeeder::class);

    }


    protected function seedUser()
    {
        $items = [
            [
                'name' => '超级系统管理员',
                'account' => 'super',
                'userType' => 0,
                'state' =>1,
                'password' => bcrypt('888888'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => '系统管理员',
                'account' => 'admin',
                'userType' =>1,
                'state' =>1,
                'password' => bcrypt('888888'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];
        DB::table('users')->insert($items);

        $items = [];
        for ($t=1;$t<=10;$t++){
            $items[] = [
                'name' => '会员'.$t,
                'account' => 'user'.$t,
                'userType' =>2,
                'password' => bcrypt('888888'),
                'state' =>1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }

        DB::table('users')->insert($items);
    }

    protected function seedArticle()
    {
        $article_columns = [
            ['id' => 1, 'name' => '每日签', 'state' => 0, 'user_id' => 1, 'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')],
            ['id' => 2, 'name' => '生活', 'state' => 0, 'user_id' => 1, 'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')],
            ['id' => 3, 'name' => '品酒', 'state' => 0, 'user_id' => 1, 'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')],
        ];

        DB::table('article_columns')->insert($article_columns);

        $arr = [
            'http://img1.3lian.com/2015/a1/91/d/240.jpg',
            'http://img1.3lian.com/2015/a1/91/d/241.jpg',
            'http://img1.3lian.com/2015/a1/91/d/242.jpg',
            'http://img1.3lian.com/2015/a1/91/d/243.jpg',
            'http://img1.3lian.com/2015/a1/91/d/244.jpg',
            'http://img1.3lian.com/2015/a1/91/d/245.jpg',
            'http://img1.3lian.com/2015/a1/91/d/246.jpg',
        ];

        $ars = [];
        foreach ($arr as $ar) {
            $m = new \App\Model\Resource();
            $arname = explode('/',$ar);
            $m->name = end($arname);
            $m->uploadType = 'url';
            $m->type = 'image/jpg';
            $m->size = 1200;
            $m->path = $ar;
            $m->save();
            $ars[] = $m;
        }

        $total = 20;

        for ($i = 1; $i <= $total; $i++) {
            $m = new \App\Model\Article();
            $m->title = '那些你想不到的设计' . $i;
            $m->author = '张三';
            $m->column_id = mt_rand(1, 3);
            $m->abstract = '摘要';
            $m->type = 'article';
            $m->content = '文章内容';
            $m->weight = 80;
            $m->pv = rand(1000, 9999);
            $m->uv = rand(1000, 9999);
            $m->user_id = 1;
            $m->online = $i % 5 === 0 ? 'no' : 'yes';
            $m->collection_quantity = rand(100, 1000);
            $m->state = 0;
            if ($m->online == "yes") {
                $m->online_at = date('Y-m-d H:i:s', time() - 24 * 60 * 60 * 30);
            } else {
                $m->offline_at = date('Y-m-d H:i:s', time() - 24 * 60 * 60 * 30);
            }
            $m->save();

            $r = rand(2, count($ars) - 1);

            for ($ii = 1; $ii <= $r; $ii++) {
                $rr = $ars[$ii];
                $ar = new \App\Model\ArticleResource();
                $ar->article_id = $m->id;
                $ar->resource_id = $rr->id;
                $ar->type = 'cover';
                $ar->save();
            }
        }

    }
}
