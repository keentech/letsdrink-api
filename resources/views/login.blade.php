<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <title>Document</title>
</head>
<script type="text/javascript">
    function getPic(){
        $.get("{{url('api/admin/captcha')}}",function (resp) {
            document.getElementById("codePic").setAttribute("src",resp);
            console.log(resp)
        });
    }

    getPic()
</script>
<body>
<form method="post" action="{{url('api/admin/auth/login')}}">
    {{csrf_field()}}
    <img id="codePic" src="" style="vertical-align:middle;cursor:pointer;"/>
    <p><input type="text" name="captcha"></p>
 <p><button type="submit" name="check">Check</button></p>
</form>
</body>
</html>