<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ExcelImportTrait;
use App\Model\SysGRegion;
use App\Model\SysRegion;
use App\Model\SysSubdivision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImportController extends BaseController
{
    use ExcelImportTrait;

    public function action(Request $request,$action)
    {
        $this->request = $request;
        $resp = $this->$action();
        return $resp;
    }

    public function data()
    {
        $file = $this->request->file('file');
        $items = $this->load($file)->toArray();
        // $rows = [];
        // foreach ($items as $key => $value) {
        //     $rows[] = ['id'=>$key+1,'name'=>$value[0],'icon'=>$value[1]];
        // }
        return response()->json($items);
    }

    public function g()
    {
        $rows = DB::table('sys_g_regions')->where('subdivision_id','=',0)->get();
        $r = [];
        foreach ($rows as $row){
            $t = DB::table('sys_subdivision')->where('subdivision','=',$row->G_Region)->first();
            $r[$row->id] = $t?$t->id:0;

        }
        dd($r);
    }

    public function gregion()
    {
        $file = $this->request->file('file');
        $items[] = $this->load($file)->toArray();

        DB::select('truncate table `sys_g_regions`');

        $i=0;
        $err = [];
        foreach ($items as $sheet) {
            $header = $sheet[0];
            if (strtolower(trim($header[0])) == "g_region_id" && strtolower(trim($header[1]))=="g_region" &&strtolower(trim($header[2]))=="ch_g_region"
                && strtolower(trim($header[3])) =="country_id") {
                unset($sheet[0]);
                foreach ($sheet as $v){
                    if($v[0] && $v[1] && $v[3]) {
                        $m = new SysGRegion();
                        $m->id = intval($v[0]);
                        $m->G_Region = trim($v[1]);
                        $m->CH_G_Region = $v[2]?trim($v[2]):'';
                        $m->country_id = intval($v[3]);
                        $t = DB::table('sys_subdivision')->where('subdivision','=',trim($v[1]))->where('country_id','=',$m->country_id)->first();
                        if ($t){
                            $m->subdivision_id = $t->id;
                        }
                        if($m->save()) $i++;
                    }
                }
            }

        }
        return $this->success(['total'=>$i,'errors'=>$err]);

    }

    public function subdivision()
    {
        $file = $this->request->file('file');
        $items = $this->load($file)->toArray();
        dd($items);

        //DB::select('truncate table sys_subdivision');

        $i=0;
        foreach ($items as $sheet) {
            foreach ($sheet as $v){
                if($v['id']) {
                    $m = SysSubdivision::where('subdivisionCode','=',$v['subdivisionCode'])->first();
                    if(!$m) $m = new SysSubdivision();
                    $m->fill($v);
                    if (!$m->chSubdivision){
                        $m->chSubdivision = $m->subdivision;
                    }
                    $m->save();
                    $i++;
                }
            }

        }
        return $this->success(['total'=>$i]);
    }

    public function region()
    {
        $file = $this->request->file('file');
        $items[] = $this->load($file)->toArray();
        DB::select('truncate table `sys_region`');
        $i=0;
        $err = [];
        foreach ($items as $sheet) {
            $header = $sheet[0];
            if (strtolower($header[0]) == "id" && strtolower($header[1])=="region" && strtolower($header[2]) =="chregion"
                && strtolower($header[3])=="regioncode"  && strtolower($header[4])=="g_region_id" &&
                strtolower(trim($header[5]))=="g_region" && strtolower($header[6])=="ch_g_region" &&
                strtolower($header[7])=="subdivision_id") {
                unset($sheet[0]);
                $regions = [];
                foreach ($sheet as $v){
                    if($v[0] && $v[7]) {
                        $m = SysRegion::where('regionCode','=',$v[3])->where('region','=',trim($v[1]))
                            ->where('subdivision_id','=',intval($v[7]))
                            ->first();
                        if ($m) {
                            $err[] = ['now'=>$v,'exist'=>$m->getAttributes()];
                            continue;
                        }
                        $g_region_id = intval($v[4]);
                        $m = new SysRegion();
                        $m->id = intval($v[0]);
                        $m->sortno = intval($v[0]);
                        $m->region = trim($v[1]);
                        $m->chRegion = $v[2]?trim($v[2]):$m->region;
                        $m->regionCode = trim($v[3]);
                        $m->subdivision_id = intval($v[7]);
                        $m->g_region_id = $g_region_id;
                        if($m->save()) {
                            if(!in_array($g_region_id,$regions) && $v[6]){
                                DB::table('sys_g_regions')->where('id','=',$g_region_id)->update(['CH_G_Region'=>trim($v[6]),'subdivision_id'=>$m->subdivision_id]);
                                array_push($regions,$g_region_id);
                            }
                            $i++;
                        }
                    }
                }
            }

        }
        $regions = array_unique($regions);
        $c = count($regions);
        return $this->success(['total'=>$i,'r'=>$c,'errors'=>$err]);
    }
}
