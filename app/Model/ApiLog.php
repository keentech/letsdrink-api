<?php

namespace App\Model;

use App\BaseModel;

class ApiLog extends BaseModel
{
    protected $table = "api_log";
}
