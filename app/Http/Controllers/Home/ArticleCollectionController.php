<?php

namespace App\Http\Controllers\Home;

use App\Model\Article;
use App\Model\ArticleCollection;
use App\Model\WeChatFromId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleCollectionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = (new ArticleCollection())
            ->where('user_id','=',$this->loginUser()->id)
            ->whereRaw('article_id not in (select id from cms_article where deleted_at is not null)');

        if ($column_id = $request->input('column')){
            $query->whereRaw('article_id in select id from article where column_id='.$column_id);
        }

        $items = $query->orderBy('created_at','desc')
            ->get();

        $items->load([
            'article' => function($q){
                $q->select('id','title','abstract','column_id','type','type','video_path','video_type');
            },
            'article.covers'
        ]);
        return $this->success($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($formid = $request->input('formid')){
            WeChatFromId::createFromForm($formid,$this->loginUser()->weixinId);
        }

        $id = $request->input('id');
        $ac = ArticleCollection::where('user_id','=',$this->loginUser()->id)
            ->where('article_id','=',$id)->first();
        if ($ac){
            return $this->failed('已收藏过此文章');
        }
        $m = new ArticleCollection();
        $m->user_id = $this->loginUser()->id;
        $m->article_id = $id;
        $m->save();

        Article::updateCollectionQuantity($id);

        return $this->message('收藏成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = ArticleCollection::with('article','article.covers')->find($id);

        return $this->success($item);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('t_user_article_collections')->where('id','=',$id)
            ->where('user_id','=',$this->loginUser()->id)
            ->delete();
        Article::updateCollectionQuantity($id);

        return $this->message('取消收藏成功');
    }

    public function cancel(Request $request)
    {
        $ids = $request->input('ids');
        if ($ids) {
            $ids = explode(',',$ids);
            DB::table('t_user_article_collections')->whereIn('article_id',$ids)
                ->where('user_id','=',$this->loginUser()->id)
                ->delete();

            Article::updateCollectionQuantity($ids);

            return $this->message('取消收藏成功');
        }
        return  $this->failed('未选择');
    }

}
