<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class UserRepository extends BaseRepository
{
    //模型名称
    protected $modelName = 'App\User';

    //基础控制器方法权限验证
    protected $allowApiMethod = ['index','show'];

    protected function query()
    {
        return $this->model()->whereNull('userType');
    }

}
