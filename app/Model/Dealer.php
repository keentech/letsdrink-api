<?php

namespace App\Model;

use App\BaseModel;

class Dealer extends BaseModel
{
    protected $table = "sys_dealers";

    protected $fillable = [
        'name',
        'tel',
        'remark',
        'area_code',
        'area_name',
        'user_id',
        'updater',
        'creator',
        'detail',
        'latitude',
        'longitude'
    ];
}
