<?php

namespace App\Helpers;

use Maatwebsite\Excel\Facades\Excel;

trait ExcelImportTrait
{
    protected function load($file)
    {
        return Excel::load($file)->all();
    }
}
