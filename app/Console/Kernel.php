<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\JobCommand::class,
        Commands\TestCommand::class,
        Commands\RepositoryCommand::class,
        Commands\FetchWeixinArticles::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        //* * * * *  cd /home/wwwroot/letsdrink-api && php artisan schedule:run >> /dev/null 2>&1
        $schedule->command('job:run runOrderSkuReturn')->everyMinute();
        $schedule->command('job:run runOrderExpireReturn')->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
