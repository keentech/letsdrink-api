<?php

namespace App\Model;

use App\BaseModel;

class ArticleColumn extends BaseModel
{
    protected $table = "cms_article_column";

    protected $fillable = [
        "name","state","user_id"
    ];

    public function articles()
    {
        return $this->hasMany('App\Model\Article','column_id','id');
    }
}
