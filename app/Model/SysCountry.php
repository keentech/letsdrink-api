<?php

namespace App\Model;

use App\BaseModel;

class SysCountry extends BaseModel
{
    protected $table = "sys_country";

    //地区
    public function subdivision()
    {
        return $this->hasMany('App\Model\SysSubdivision', 'country_id', 'id');
    }

}