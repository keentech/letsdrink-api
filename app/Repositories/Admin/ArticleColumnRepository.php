<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class ArticleColumnRepository extends BaseRepository
{
    protected $modelName = "App\Model\ArticleColumn";

    protected function relationLoad()
    {
        return [];
    }
}
