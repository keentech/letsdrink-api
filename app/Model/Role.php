<?php

namespace App\Model;

use App\BaseModel;

class Role extends BaseModel
{
    protected $table = 't_role';
}
