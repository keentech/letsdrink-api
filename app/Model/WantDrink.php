<?php

namespace App\Model;

use App\BaseModel;

class WantDrink extends BaseModel
{
    protected $table = "t_user_want_drinks";

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\WineProduct','product_id','id');
    }
}
