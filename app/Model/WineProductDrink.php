<?php

namespace App\Model;

use App\BaseModel;

class WineProductDrink extends BaseModel
{
    protected $table = "wine_product_drink";

    protected $appends = ['tasteKeywords'];

    protected $hidden = ['keywords'];

    protected $fillable = [
        'product_id',
        'consume',
        'wineglass',
        'sober_time',
        'temperature',
        'chinese_food',
        'western_food',
        'snack',
        'delicacy',
        'undelicacy',
        'talk_about',
        'updater',
        'creator',
        'user_id',
        "talk_keywords",
    ];

    //味道关键词
    public function keywords()
    {
        return $this->hasMany('App\Model\WineDrinkKeywords','drink_id','id');
    }

    public function getTasteKeywordsAttribute()
    {
        return $this->keywords;
    }
}
