<?php

use Illuminate\Database\Seeder;

class SetingConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['key' => 'new_user_coupon', 'label' => '新注册用户优惠券礼包' , 'value' => '','show_frontend' => 0],
            ['key' => 'invite_user_coupon', 'label' => '邀请新人获得的优惠券礼包' , 'value' => '','show_frontend' => 0],
            ['key' => 'day_user_coupon', 'label' => '每日红包' , 'value' => '','show_frontend' => 0],
        ];

        \Illuminate\Support\Facades\DB::table('sys_seting_configs')->insert($data);
    }
}
