<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_coupons', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('code')->comment('优惠券编码');
            $table->integer('get_user_id')->comment('领取人');
            $table->integer('coupon_id')->comment('优惠券id');
            //$table->enum('has_used',['no','yes'])->comment('是否已使用no未使用|yes已使用');
            $table->date('used_at')->nullable()->comment('使用日期');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_coupons');
    }
}
