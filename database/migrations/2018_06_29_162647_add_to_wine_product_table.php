<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToWineProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wine_product', function (Blueprint $table) {
            $table->string('capacity')->comment('产品容量');
            $table->integer('show_scenes')->default(1)->comment("是否显示消费场景:0不显示|1显示");
            $table->integer('show_cuisine')->default(1)->comment("是否显示菜系:0不显示|1显示");
            $table->integer('taste_id')->comment('口味定义');
            $table->integer('grading_id')->comment('产品档次');
            $table->integer('g_region_id')->comment('大区id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wine_product', function (Blueprint $table) {
            //
        });
    }
}
