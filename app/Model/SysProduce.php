<?php

namespace App\Model;

use App\BaseModel;

class SysProduce extends BaseModel
{
    protected $table = "sys_produce";

    protected $fillable = [
        'name',
        'chname',
        'logo',
        'produceCode',
        'remark',
        'user_id',
    ];
}
