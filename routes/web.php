<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $m = new \App\Helpers\ProductPoster();
//    $s = $m->createByProduct(13,1);
//    echo $s;
//    echo "<img src='$s'>";
    return view('welcome');
});
Route::get('/t', 'Home\MyController@test');
