<?php

use Illuminate\Database\Seeder;

class SceneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //消费场景
        $items = [
            [
                "name" => "我要送礼" ,
                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                "child" => [
                    [
                        "name" => "生日礼物" ,
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                        "child" => [
                            [
                                "name" => "送领导" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送闺蜜" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送基友" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送长辈" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送客户" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送情侣" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ]
                        ]
                    ],
                    [
                        "name" => "节日送礼" ,
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                        "child" => [
                            [
                                "name" => "送亲戚" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送闺蜜" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送基友" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送领导" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "送客户" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ]
                        ]
                    ],
                    [
                        "name" => "求人帮忙" ,
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                        "child" => []
                    ],
                    [
                        "name" => "私人拜访" ,
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                        "child" => [
                            [
                                "name" => "拜访长辈" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "拜访客户" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ],
                            [
                                "name" => "拜访领导" ,
                                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_like@2x.png",
                                "child" => []
                            ]
                        ]
                    ],
                ]
            ],
            [
                "name" => "我有宴请",
                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/Group6@2x.png",
                "child" => [
                    [
                        "name" => "商务宴请",
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/Group6@2x.png",
                        "child" => [

                        ]
                    ],
                    [
                        "name" => "公司聚餐",
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/Group6@2x.png",
                        "child" => [

                        ]
                    ],
                    [
                        "name" => "婚宴",
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/Group6@2x.png",
                        "child" => [

                        ]
                    ],
                    [
                        "name" => "家宴",
                        "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/Group6@2x.png",
                        "child" => [

                        ]
                    ]

                ]
            ],
            [
                "name" => "朋友聚会",
                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/Group7@2x.png",
                "child" => [

                ]
            ],
            [
                "name" => "一个人喝",
                "icon" => "http://cdn.letsdrink.com.cn/letsDrink/image/icon/general_img_lik@2x.png",
                "child" => [

                ]
            ]
        ];

        function save($item,$parent_id=0){
            $m = new \App\Model\WineProductScene();
            $m->name = $item['name'];
            $m->icon = $item['icon'];
            $m->parent_id = $parent_id;
            $m->save();
            if (count($item['child'])>0){
                foreach ($item['child'] as $child){
                    save($child,$m->id);
                }
            }
        }

        \Illuminate\Support\Facades\DB::select('truncate table `wine_product_scene_relation`');
        \Illuminate\Support\Facades\DB::select('truncate table `wine_product_scene`');

        foreach ($items as $k=>$item){
            save($item,0);
        }

    }
}
