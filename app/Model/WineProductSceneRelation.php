<?php

namespace App\Model;

use App\BaseModel;

class WineProductSceneRelation extends BaseModel
{
    protected $table = 'wine_product_scene_relation';

    protected $fillable = [
        'product_id',
        'scene_id',
        'user_id',
        'scene_one_id',
        'scene_two_id',
        'scene_three_id'
    ];

    protected $appends = ['currentScene'];

    protected $visible = ['id','scene_one_id','scene_two_id','scene_three_id','currentScene'];

    //获取最后一个标签
    public function getCurrentSceneAttribute()
    {
        $id = $this->scene_three_id?$this->scene_three_id:($this->scene_two_id?$this->scene_two_id:$this->scene_one_id);
        return WineProductScene::find($id);
    }
}
