<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class WineGradingRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineGrading";
}