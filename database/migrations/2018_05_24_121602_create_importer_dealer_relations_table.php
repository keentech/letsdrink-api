<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImporterDealerRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_importer_dealer_relation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('importer_id');
            $table->integer('dealer_id');
            $table->string('remark');
            $table->integer('user_id');
            $table->string('updater');
            $table->string('creator');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_importer_dealer_relation');
    }
}
