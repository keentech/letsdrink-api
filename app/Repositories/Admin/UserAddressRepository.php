<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class UserAddressRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\UserAddress";
}