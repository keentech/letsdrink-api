<?php

namespace App\Model;

use App\BaseModel;

class SysRegion extends BaseModel
{
    protected $table = "sys_region";

    protected $fillable = [
        'sortno',
        'region',
        'chRegion',
        'regionCode',
        'subdivision_id',
        'creator',
        'updater',
        'remark',
        'user_id',
    ];

    //地区
    public function subdivision()
    {
        return $this->belongsTo('App\Model\SysSubdivision','subdivision_id','id');
    }
}
