<?php

namespace App\Helpers;

/*
 * 日志记录
 *
 *
 */

trait ApiAdminLogTrait
{
    protected function saveIndexLog($request)
    {
        $log = ['action'=>' ','content'=>' ','request'=>$request->all()];

        $this->saveLog($log);

    }
    protected function saveShowLog($id)
    {
        $log = ['action'=>'show','content'=>' ','request'=>$id];
        $this->saveLog($log);
    }
    protected function saveStoreLog($request,$model)
    {
        $log = ['action'=>'create','content'=>' ','request'=>$request->all()];
        $this->saveLog($log);
    }

    protected function saveUpdateLog($model,$originalModel,$request,$id)
    {
        $log = ['action'=>'update','content'=>$id,'request'=>$request->all()];
        $this->saveLog($log);
    }

    protected function saveDestroyLog($model,$id)
    {
        $log = ['action'=>'delete','content'=>' ','request'=>$id];
        $this->saveLog($log);
    }

    protected function saveDestroyBatchLog($request)
    {
        $log = ['action'=>'delete','content'=>' ','request'=>$request->all()];
        $this->saveLog($log);
    }

    protected function saveLog($log)
    {
        Helpers::log(is_string($log)?$log:json_encode($log),'api_admin_log');
    }
}
