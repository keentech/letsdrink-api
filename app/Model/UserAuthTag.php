<?php

namespace App\Model;

use App\BaseModel;
use Illuminate\Support\Facades\DB;

class UserAuthTag extends BaseModel
{
    protected $table = "t_user_auth_tags";

    public static function codeList()
    {
        return self::pluck('code')->toArray();
    }

    public static function getNameByCode($code)
    {
        $item = DB::table('t_user_auth_tags')->where('code','=',$code)->first();
        if ($item) return $item->name;
        return $code;
    }
}
