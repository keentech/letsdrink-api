<?php

namespace App\Http\Controllers\Home;

use App\Helpers\YouZanTrait;
use Illuminate\Http\Request;

class YouZanController extends BaseController
{
    use YouZanTrait;

    //获取店铺信息
    public function shop()
    {
        return $this->success($this->getShop());
    }

    //获取商品详细
    public function item($id)
    {
        return $this->success($this->getItem($id));
    }

    //获取出售中商品列表
    public function onSale()
    {
        return $this->success($this->getItemsOnSale());
    }

    //获取仓库中商品列表
    public function inventory()
    {
        return $this->success($this->getItemsInventory());
    }

    //获取商品类目列表
    public function itemCategories()
    {
        return $this->success($this->getItemCategories());
    }

    //根据是否排序查询商品分组列表
    public function itemCategoriesTags()
    {
        return $this->success($this->getItemCategoriesTags());
    }

    //使用手机号获取用户openId
    public function userWeixinOpenid(Request $request)
    {
        $params['mobile'] = $request->input('mobile');
        $params['country_code'] = $request->input('country_code','+86');
        return $this->success($this->getUserWeixinOpenid($params));
    }

    //根据店铺信息、身份、成为客户/会员的时间等条件获取客户列表
    public function scrmCustomerSearch(Request $request)
    {
        $params = [];
        return $this->success($this->getScrmCustomerSearch($params));
    }

    //查询卖家已卖出的交易列表
    public function tradesSold(Request $request)
    {
        $params = [];
        return $this->success($this->getTradesSold($params));
    }

    //通过订单号获取物流信息
    public function LogisticsExpressByOrderNo($tid)
    {
        return $this->success($this->getLogisticsExpressByOrderNo($tid));
    }

    //获取物流快递信息
    public function logisticsGoodsExpress(Request $request)
    {
        $params['express_id'] = $request->input('express_id');//快递公司id
        $params['express_no'] = $request->input('express_no');//物流单号
        return $this->success($this->getLogisticsGoodsExpress($params));
    }

}
