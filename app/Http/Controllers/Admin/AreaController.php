<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Model\Area;
use Illuminate\Http\Request;

class AreaController extends BaseController
{
    public function index()
    {
        if(request('new') == "true"){
            $items = Area::saveAsJsonFile2();
        }else{
            $items = Helpers::getTempJson('areas2',true);
            if (!$items){
                $items = Area::saveAsJsonFile2();
            }
        }
        return response()->json($items);

    }
}
