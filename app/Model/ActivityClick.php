<?php

namespace App\Model;

use App\BaseModel;

class ActivityClick extends BaseModel
{
    protected $table = "activity_clicks";
}
