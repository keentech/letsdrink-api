<?php

namespace App\Console\Commands;

use App\Helpers\Helpers;
use App\Model\Article;
use App\Model\OrderPayRecord;
use App\Model\UserAuthTag;
use App\Model\UserCoupon;
use App\Model\UserRole;
use App\Model\WineCoupon;
use App\Model\WineProduct;
use App\Model\WineProductDealerRelation;
use App\Model\WineProductImporterRelation;
use App\Model\WineProductScene;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 't:run {type} {--table=} {--password=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        $arr = get_class_methods(__CLASS__);
        $this->log('run command_'.$type);

        if(in_array('command_'.$type,$arr)) {
            $cmd = 'command_'.$type;
            $this->line(date('Y-m-d H:i:s').' run: '.$cmd);
            $this->$cmd();
            $this->line('done');
            $this->log('finished command_'.$type);
        }else{
            $this->command_notice();
            $this->line('not has '.$type.' command');
        }
    }

    //日志存放在job_command_log
    private function log($data)
    {
        Helpers::log($data,'test_command_log');
    }

    public function command_notice()
    {
        $this->line('1.notice          >       notice');
    }

    private function command_update()
    {

    }

    private function command_create_super()
    {
        $u = new User();
        $u->name = "super";
        $u->account = "super";
        $u->userType = "super";
        $u->state = 1;
        $u->password = bcrypt(888888);
        $u->save();

        $r = new UserRole();
        $r->user_id = $u->id;
        $r->role_id = 1;
        $r->save();
    }

    private function command_all_user_password_rest()
    {
        $password = $this->option('password');
        if (!$password) $password = 888888;
        $rows = User::all();
        foreach ($rows as $row) {
            $row->password = bcrypt($password);
            $row->save();
        }
    }

    private function command_db()
    {
        $path = base_path().'/db/'. date('Ymd_His').'.sql';
        $cmd = 'mysqldump -u'.env('DB_USERNAME').' -p'.env('DB_PASSWORD').' '.env('DB_DATABASE').'>'.$path;

        exec($cmd);
    }

    private function command_updateCollectionQuantity()
    {
        $rows = DB::table('cms_article')->pluck('id')->toArray();
        Article::updateCollectionQuantity($rows);
    }

    private function command_publish()
    {
        DB::select('update `wine_product` set state="publish"');
        DB::select('update `wine_chateau` set state="publish"');
    }

    private function command_coupon()
    {
        foreach (WineCoupon::all() as $v){
            $v->createCode();
        }
    }
    private function ccommand_kw()
    {

        $storage_path =  storage_path().'/app/TasteKeywords/';
        $dirs = scandir($storage_path);
        unset($dirs[0]);
        unset($dirs[1]);
        $rows = [];
        foreach ($dirs as $v){
            if (preg_match('/(.*?)@2x\.png/i',$v,$arr)){
                $name = $arr[1];
                $file = $storage_path.'/'.$v;
                $path = 'letsDrink/image/'.date('Ymd') .'/'.str_random(64).'.png';
                $disk = Storage::disk('qiniu');
                $disk->put($path,file_get_contents($file));
                $r_path = "http://cdn.letsdrink.com.cn/".$path;
                $rows[] = ['name'=>$name,'icon'=>$r_path];
            }

        }

        Helpers::saveTempJson($rows,'TasteKeywords');

    }

    private function command_express_sort()
    {
        DB::select('UPDATE `express_carriers` SET `sort`=0');
        DB::select('UPDATE `express_carriers` SET `sort`=100 WHERE id=66');
        DB::select('UPDATE `express_carriers` SET `sort`=100 WHERE id=67');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=68');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=69');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=70');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=71');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=72');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=73');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=74');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=75');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=80');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=190');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=244');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=253');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=5');
        DB::select('UPDATE `express_carriers` SET `sort`=50 WHERE id=6');
    }

    private function command_t()
    {
        foreach (WineCoupon::all() as $c){
          $c->get = DB::table('t_user_coupons')->where('coupon_id','=',$c->id)->count();
          $c->save();
        }
    }

    protected function command_order_count()
    {
        $rows = DB::table('orders')->groupBy('create_user_id')->select('create_user_id')->get();

        foreach ($rows as $row){
            User::updateOrderCountByUserId($row->create_user_id);
        }
    }
}
