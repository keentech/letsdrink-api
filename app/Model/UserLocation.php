<?php

namespace App\Model;

use App\BaseModel;

class UserLocation extends BaseModel
{
    protected $table = 't_user_locations';

    protected $fillable = [
    	'origin',
		'latitude',
		'longitude',
		'speed',
		'accuracy',
		'altitude',
		'verticalAccuracy',
		'horizontalAccuracy',
    ];
}
