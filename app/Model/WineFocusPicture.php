<?php

namespace App\Model;

use App\BaseModel;

class WineFocusPicture extends BaseModel
{
    protected $table = 'wine_focus_picture';

    protected $fillable = [
        'title',
        'pic',
        'obj_id',
        'weight',
        'type',
        'user_id',
        'creator',
        'updater',
    ];

    public function object()
    {
        $models = [
            'article' => 'App\Model\Article',
            'product' => 'App\Model\WineProduct',
            'activity'=> 'App\Model\Activity',
        ];

        return $this->belongsTo($models[$this->type],'obj_id','id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
