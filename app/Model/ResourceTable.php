<?php

namespace App\Model;

use App\BaseModel;

class ResourceTable extends BaseModel
{
    protected $table = "sys_resource_table";
}
