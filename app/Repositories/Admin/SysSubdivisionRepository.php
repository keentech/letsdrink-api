<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class SysSubdivisionRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\SysSubdivision";

    protected $allowApiMethod = ['index','show'];

    protected function relationLoad()
    {
        return ['country'];
    }

    protected function getShowRespondData($model)
    {
        return $model->load('country','region');
    }

    public function export()
    {
        $data = $this->query()->orderBy('id','asc')->select(['id','subdivision','chSubdivision','subdivisionCode','country_id'])
            ->get()->toArray();
        return $this->exportFile('地区码模板',$data);
    }
}