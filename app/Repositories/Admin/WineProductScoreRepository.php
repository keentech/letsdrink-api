<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use App\Model\WineProduct;
use App\Model\WineProductExpertScore;
use App\Model\WineProductScore;
use App\User;
use Illuminate\Support\Facades\DB;

class WineProductScoreRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineProductScore";

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    //在列表显示之前
    protected function initListQuery($query)
    {
        return $query->where('type','=','self');
    }

    protected function relationLoad()
    {
        return ['sameScores'];
    }

    protected function getShowRespondData($model)
    {
        return $model->load('sameScores');
    }

    protected function commonRules()
    {
        return [
            'product_id' => 'required',
            'same_arr' => 'sometimes|max:10'
        ];
    }

    protected function processStoreValidate()
    {
        $c = $this->query()->where('product_id','=',$this->request->input('product_id'))->count();
        if($c > 0) {
            $this->error = '记录已存在，请勿重复添加！';
            return false;
        }
        return true;
    }

    protected function appendCommonFillData()
    {
        $m = WineProduct::find($this->request->input('product_id'));
        if($m && $m->vintage){
            return ['vintage'=>$m->vintage,'type'=>'self'];
        }
        return [];
    }

    protected function commonSave($model)
    {
        $user_id = $this->loginUser->id;

        //本款酒评分
        if ($this->request->has('self_arr')) {
            DB::table('wine_product_expert_score')->where('score_id','=',$model->id)->delete();
            $items = $this->request->input('self_arr');
            $items = $items?$items:[];
            foreach ($items as $item) {
                $m = new WineProductExpertScore();
                $m->score_id = $model->id;
                $m->product_id = $model->product_id;
                $m->user_id = $user_id;
                $m->expert_user_id = $item['expert_user_id'];
                if($expert = User::find($item['expert_user_id'])){
                    $m->expert_user_name = $expert->name;
                }
                $m->score = $item['score'];
                $m->save();
            }

            $model->updateAverageScore();
        }

        //同系列酒
        if ($this->request->has('same_arr')) {
            DB::table('wine_product_score')->where('type','=','same')
                ->where('product_id','=',$model->product_id)
                ->delete();
            DB::table('wine_product_expert_score')->where('product_id','=',$model->product_id)
                ->where('score_id','<>',$model->id)->delete();

            $items = $this->request->input('same_arr');

            $items = $items?$items:[];

            foreach ($items as $item) {

                $w = new  WineProductScore();
                $w->type = 'same';
                $w->user_id = $user_id;
                $w->vintage = $item['vintage'];
                $w->product_id = $model->product_id;
                if($w->save()) {
                    foreach ($item['items'] as $row) {
                        $m = new WineProductExpertScore();
                        $m->score_id = $w->id;
                        $m->product_id = $w->product_id;
                        $m->user_id = $user_id;
                        $m->expert_user_id = $row['expert_user_id'];
                        if($expert = User::find($row['expert_user_id'])){
                            $m->expert_user_name = $expert->name;
                        }
                        $m->score = $row['score'];
                        $m->save();
                    }
                }
                $w->updateAverageScore();
            }
        }
    }

    private function saveScore()
    {

    }

}