<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Mem Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('logout', 'AuthController@logout');
    Route::group(['middleware'=>'memJWT'],function () {
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
        Route::post('me-update', 'AuthController@userInfoUpdate');
        Route::post('me-new', 'VisitController@newUser');//新增用户
    });
});

Route::resource('article','ArticleController');     //文章
Route::resource('dailySign','DailySignController'); //每日签
Route::get('today-daily-sign','DailySignController@today'); //每日签
Route::get('drink-hot','DrinkController@hot');      //酒最热卖
Route::resource('drink','DrinkController');         //酒
Route::resource('chateau','ChateauController');     //酒庄
Route::get('drink-focus','DrinkFocusController@index');   //焦点图
Route::get('frontend-seting','BaseController@frontendSeting');   //参数设置
Route::get('today-potable','DrinkController@todayPotable');   //今日适饮
Route::get('search','SearchController@search');         //搜索
Route::get('poster/{id}','DrinkController@getPoster');         //二维码

Route::group(['middleware'=>'memJWT'],function (){

    Route::post('visit','VisitController@store');//访问日志
    Route::get('drink/{id}/poster','DrinkController@poster');//酒海报
    Route::get('article/{id}/poster','ArticleController@poster');//文章海报

    Route::resource('my','MyController');               //我的信息
    Route::resource('myDrink','MyDrinkController');     //我的酒

    Route::resource('has-drink','HasDrinkController');//喝过的酒
    Route::resource('want-drink','WantDrinkController');//想喝的酒
    Route::resource('article-collect','ArticleCollectionController');//收藏的文章
    Route::resource('address','AddressController');//收货地址
    Route::resource('location','LocationController');//定位地址
    Route::post('address-default/{id}','AddressController@setDefault');//收货地址

    Route::post('has-drink-cancel','HasDrinkController@cancel');//删除喝过的酒
    Route::post('want-drink-cancel','WantDrinkController@cancel');//删除想喝的酒
    Route::post('article-collect-cancel','ArticleCollectionController@cancel');//删除收藏的文章

    Route::any('drink-from-code','DrinkController@getProductFromQrCode');//从二维码中获取商品

    Route::get('gift-card','GiftController@index');//礼物卡
    Route::get('gift-card-2','GiftController@index2');//礼物卡2

    Route::get('product-coupon/{id}','CouponController@productCoupon');//优惠券列表
    Route::resource('coupon','CouponController');//优惠券列表
    Route::get('expiring','MeController@expiring');//即将到期的礼物和优惠券


    //订单模块
    Route::resource('order','OrderController');//订单
    Route::post('order/{id}/cancel','OrderController@cancel');//订单
    Route::post('order-delete/{id}','OrderController@delete');//订单删除
    Route::post('gift-give','OrderController@giftOfGive');//赠送礼物
    Route::get('gift-show','OrderController@giftOfShow');//显示礼物
    Route::post('gift-get','OrderController@giftOfGet');//领取礼物
    Route::post('gift-resend/{id}','OrderController@giftOfResend');//重新赠送
    Route::post('order-complete/{id}','OrderController@complete');//确认收获
    Route::get('express/{id}','OrderController@express');//订单物流

    Route::resource('activity','ActivityController');      //我的活动
    Route::get('activity/{id}/detail','ActivityController@detail');      //活动
    Route::post('activity/{id}/help','ActivityController@freeHelp');      //活动帮助
    Route::post('activity/{id}/complete','ActivityController@complete');      //确认领取活动
    Route::any('formid','ActivityController@formid');      //保存form_id用于发送消息

});