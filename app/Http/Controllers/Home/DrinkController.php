<?php

namespace App\Http\Controllers\Home;

use App\Helpers\ProductPoster;
use App\Model\UserCoupon;
use App\Model\UserScan;
use App\Model\Visit;
use App\Model\WineProduct;
use App\Model\WineQrcodeRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DrinkController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = new WineProduct();

        if ($request->has('scene_id')){
            $scene_id = $request->input('scene_id',0);
            $query = $query->whereRaw('`id` in (select product_id from wine_product_scene_relation where scene_one_id='.$scene_id.'
            or scene_two_id='.$scene_id.' or scene_three_id='.$scene_id.')');
        }


        $items = $query->where('online','=',"yes")
            ->orderBy('weight','desc')
            ->orderBy('created_at','desc')
            ->with(['userHasDrink','userWantDrink',"wineGrading","scenes",'drinkTasteKeywords','cuisines','wineTastes'])
            ->select('id','name','chname','chateauName','vintage','banner','price','promotion_price',
                'show_cuisine','show_scenes','letDrink_recom','grading_id','taste_id')
            ->paginate(15);


        return $this->success($items);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $item = WineProduct::find($id);
        if($item) {
            $this->visit($id,2,$item);

            $item->showDetail();
            if (\request('from') == "qrcode") {
                $this->visit($item->id,2,$item,1);
            }

            return $this->success($item);
        }

        return $this->notFond();


    }

    //从二维码中获取商品
    public function getProductFromQrCode(Request $request)
    {
        $code = $request->input('code');
        $WQR  = WineQrcodeRecord::findByCode($code);
        if ($WQR) {
            if($wineProduct = WineProduct::find($WQR->product_id)){

                $wineProduct->showDetail();

                $this->visit($wineProduct->id,2,$wineProduct,1);

                return $this->respond(['status'=>'success','code'=>200,'qrcode'=>$code,'data'=>[$wineProduct]]);
            }
        }

        return $this->notFond();
    }

    //最热卖
    public function hot()
    {
        $max = DB::select('SELECT MAX(total),product_id FROM (SELECT COUNT(id) AS total,product_id 
        FROM orders GROUP BY product_id) con');

        if (count($max)>0){
            $item = WineProduct::with(['userHasDrink','userWantDrink',"wineGrading","scenes",'drinkTasteKeywords','cuisines','wineTastes'])
            ->select('id','name','chname','chateauName','vintage','banner','price','promotion_price',
                'show_cuisine','show_scenes','letDrink_recom','grading_id','taste_id')
            ->find($max[0]->product_id);
            return $this->success($item);
        }

        return $this->success(null);

    }

    //今日适饮
    public function todayPotable()
    {
        $item = DB::table('wine_today_potables')->select('title','online_at')->where('online_at','<=',date('Y-m-d'))
            ->orderBy('online_at','desc')
            ->first();
        return $this->success($item);

    }

    public function poster(Request $request,$id)
    {
        $username = $request->input('username');
        if (!$username){
            $username = $this->loginUser()->name;
        }
        $ProductPoster = new  ProductPoster();
        $path = $ProductPoster->createByProduct($id,$this->loginUserId(),$username);
        if ($path) {
            return $this->success($path);
        }
        return false;
    }

    public function getPoster($id)
    {
        $item = DB::table('t_user_poster')->select('id','obj_id','type','path')->find($id);
        $m = new UserScan();
        $m->user_id = $this->loginUserId();
        $m->obj_id = $id;
        $m->obj_type = "poster";
        $m->save();
        return $this->success($item);
    }
}
