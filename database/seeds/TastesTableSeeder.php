<?php

use Illuminate\Database\Seeder;

class TastesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //产品口味
        $items = [
            ['id'=>1, 'name' => '重口味'],
            ['id'=>2, 'name' => '小甜水'],
            ['id'=>3, 'name' => '高级感'],
            ['id'=>4, 'name' => '甜美系'],
            ['id'=>5, 'name' => '清爽系'],
            ['id'=>6, 'name' => '酸爽系'],
            ['id'=>7, 'name' => '水果炸弹'],
            ['id'=>8, 'name' => '烟草系'],
        ];

        \Illuminate\Support\Facades\DB::table('wine_tastes')->insert($items);
    }
}
