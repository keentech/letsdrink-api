<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ApiListTrait;
use App\Helpers\ExcelExportTrait;
use App\Helpers\Trackingmore;
use App\Model\Express;
use App\Model\ExpressCarrier;
use App\Model\Order;
use App\Model\WeChatNotify;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OrderController extends BaseController
{
    use ApiListTrait,ExcelExportTrait;
    //模型名称
    protected $modelName = "App\Model\Order";

    //创建查询模型
    protected function query()
    {
        return $this->model();
    }

    //实例化模型
    protected function model($id = false)
    {
        return $id ? $this->query()->find($id) : new $this->modelName;
    }

    //模型字段
    protected function modelColumns()
    {
        return $this->model()->tableColumns();
    }

    //发货
    public function delivery(Request $request)
    {
        $rules = [
            'out_trade_no' =>[
                'required',
                Rule::exists('orders','out_trade_no')->where(function ($query){
                    $query->where('state',1);
                })
            ] ,
            'code' => 'required',
            'number' => 'required'
        ];

        $messages = [
            'out_trade_no.exists' => '商品已发货，请勿重复',
            'number' => "物流单号必填",
            'code' => "物流公司必选"
        ];

        if (!$this->requestValidate($request->all(),$rules,$messages)){
            return $this->failed($this->error);
        }

        $number = $request->input('number');
        $code = $request->input('code');
        $out_trade_no = $request->input('out_trade_no');
        $c  = Express::where('tracking_number','=',$number)->where('carrier_code','=',$code)
            ->where('out_trade_no','=',$out_trade_no)->count();

        if ($c > 0 )  return $this->failed('已存在，请勿重复添加！');

        $express = Express::firstOrNew(['tracking_number'=>$number,
            'carrier_code'=>$code,
            'out_trade_no'=>$out_trade_no]);



        $order = Order::where('out_trade_no','=',$request->input('out_trade_no'))->first();

        $express->order_id = $order->id;
        $express->title = $order->product_chname?$order->product_chname:'';
        $express->customer_name = $order->username?$order->username:'';

        $order->is_delivery = "yes";
        $order->delivery_time = date('Y-m-d H:i:s');
        $order->state = 5;
        $order->save();

        $express->save();

        ExpressCarrier::addSort($code);

        $t = new Trackingmore();
        $tracking = [
            'title'=>$express->title,
            'customer_name' => $express->customer_name,
            'customer_email' => '',
            'order_id'=>$express->out_trade_no
        ];
        $t->createTracking($express->carrier_code,$express->tracking_number,$tracking);

        //发送订单发货提醒模板消息
        $m = new WeChatNotify();
        $m->orderDelivery($express);

        return $this->success($express);
    }

    //查物流
    public function queryDelivery($id)
    {
        $order = Order::find($id);

        if (!$order) return $this->notFond();

        $e = Express::where('out_trade_no','=',$order->out_trade_no)
            ->first();

        if (!$e)  return $this->notFond();

        $e->updateTrackingRealTime();

        $e->load('carrier');

        return $this->success($e);

    }

    public function export(Request $request)
    {
        $query = new Order();
        $this->build($query,$request->input('_param'));

        $this->processFilter();//过滤

        $this->processSearch();//搜索

        $this->processSort();//过滤

        $this->processPages();//分页

        $query = $this->processQueryModel;

        $items = $query->orderBy('created_at','desc')->get();

        $rows = $items->map(function ($item){
            $d = [];
            $d['订单号'] = $item->out_trade_no;
            $d['来源'] = $item->origin=="general"?"普通":$item->origin_qrcode;
            $d['付款人'] = $item->createUser?$item->createUser->name:'';
            $d['收礼人'] = $item->getUser?$item->getUser->name:$item->username;
            $d['葡萄酒'] = $item->product_chname;
            $d['单价'] = $item->price;
            $d['数量'] = $item->quantity;
            $d['优惠券金额'] = $item->coupon_fee;
            $d['总价'] = $item->total_fee;
            $d['类型'] = $item->type=="gift"?"赠礼订单":"普通订单";
            $d['收件人名称'] = $item->username;
            $d['收件人手机'] = $item->tel_number;
            $d['收件人地址'] = $item->address;
            $d['状态'] = $item->chState;
            $d['下单时间'] = is_object($item->created_at)?$item->created_at->toDateTimeString():$item->created_at;
            $d['买家留言'] = $item->comment;

            return $d;
        });
        $name = "订单-".date('Ymd');

        $style['width'] = array(
            'A' => 30,
            'B' => 55,
            'C' => 20,
            'D' => 20,
            'E' => 30,
            'F' => 15,
            'G' => 15,
            'H' => 15,
            'I' => 15,
            'J' => 15,
            'K' => 20,
            'L' => 25,
            'M' => 50,
            'N' => 15,
            'O' => 20,
            'P' => 55,
          
        );
        return $this->exportFile($name,$rows,'xlsx',$style);
    }
}
