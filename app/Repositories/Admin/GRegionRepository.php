<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class GRegionRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\SysGRegion";

    protected $allowApiMethod = ['index'];

    protected function beforeListShow($model)
    {
        return $model->orderBy('id','asc');
    }

    public function export()
    {
        $data = $this->query()->orderBy('id','asc')->select(['id','G_Region','CH_G_Region','country_id','subdivision_id'])
            ->get()->toArray();
        return $this->exportFile('大区码模板',$data);
    }
}