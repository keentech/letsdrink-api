<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class BaseModel extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected function loginUser()
    {
        return auth()->user();
    }

    protected $guarded = ['_model','deleted_at'];
    //获取表字段
    public function tableColumns()
    {
        return Schema::getColumnListing($this->getTable());
    }

    public function beforeSave($input=null)
    {
        return true;
    }

    public function afterSave($input=null)
    {
        return true;
    }

    public function scopeWithOnly($query, $relation, Array $columns = [])
    {
        return $query->with([$relation => function ($query) use ($columns){
            if(!$columns) $columns = '*';
            $query->select($columns);
        }]);
    }
}
