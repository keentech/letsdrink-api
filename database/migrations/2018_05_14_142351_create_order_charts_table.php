<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderChartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_charts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->comment('日期');
            $table->integer('total')->default(0)->comment('全部订单量');
            $table->decimal('total_fee',12,2)->default(0)->comment('订单总金额');
            $table->integer('general_quantity')->default(0)->comment('普通订单成交量');
            $table->decimal('general_fee',12,2)->default(0)->comment('普通订单成交总金额');
            $table->integer('gift_quantity')->default(0)->comment('赠礼订单成交量');
            $table->decimal('gift_fee',12,2)->default(0)->comment('赠礼订单成交总金额');
            $table->integer('qr_general_quantity')->default(0)->comment('二维码来源普通订单');
            $table->decimal('qr_general_fee',12,2)->default(0)->comment('二维码来源普通订单总金额');
            $table->integer('qr_gift_quantity')->default(0)->comment('二维码来源赠礼订单');
            $table->decimal('qr_gift_fee',12,2)->default(0)->comment('二维码来源赠礼订单总金额');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_charts');
    }
}
