<?php

namespace App\Helpers;


use Illuminate\Support\Facades\Validator;

trait ApiCurdTrait
{
    //存储验证规则
    protected function storeRules()
    {
        return [];
    }

    //更新验证规则
    protected function updateRules($id)
    {
        return [];
    }

    //公共的规则（存储和更新）
    protected function commonRules()
    {
        return [];
    }
    //公共的错误消息
    protected function commonMessages()
    {
        return [];
    }

    protected function commonSave($model)
    {
        return true;
    }
    //删除验证规则
    protected function destroyRules($id)
    {
        return [];
    }

    //批量删除消息
    protected function destroyBatchRules()
    {
        return [
            'ids' => 'required',
        ];
    }

    //存储消息
    protected function storeMessages()
    {
        return [];
    }

    //更新消息
    protected function updateMessages()
    {
        return [];
    }

    //删除消息
    protected function destroyMessages()
    {
        return [];
    }

    //批量删除消息
    protected function destroyBatchMessages()
    {
        return [];
    }

    //基础存储验证
    protected function baseStoreValidate()
    {
        return $this->requestValidate($this->storeRules(),array_merge($this->commonMessages(),$this->storeMessages()));
    }

    //基础更新验证
    protected function baseUpdateValidate($id)
    {
        return $this->requestValidate($this->updateRules($id),array_merge($this->commonMessages(),$this->updateMessages()));
    }

    //基础删除验证
    protected function baseDestroyValidate($id)
    {
        return $this->requestValidate($this->destroyRules($id),array_merge($this->commonMessages(),$this->destroyMessages()));
    }

    //基础批量删除验证
    protected function baseDestroyBatchValidate()
    {
        return $this->requestValidate($this->destroyBatchRules(),array_merge($this->commonMessages(),$this->destroyBatchMessages()));
    }

    //新建更新公共验证部分
    protected function commonValidate()
    {
        return $this->requestValidate($this->commonRules(),$this->commonMessages());
    }

    //自定义存储验证
    protected function processStoreValidate()
    {
        return true;
    }

    //自定义更新验证
    protected function processUpdateValidate($id)
    {
        return true;
    }

    //自定义删除验证
    protected function processDestroyValidate($id)
    {
        return true;
    }

    //自定义删除验证
    protected function processDestroyBatchValidate()
    {
        return true;
    }

    //存储验证
    protected function storeValidate()
    {
        if(!$this->commonValidate()) {
            return false;
        }

        if(!$this->baseStoreValidate()) {
            return false;
        }

        return $this->processStoreValidate();
    }

    //更新验证
    protected function updateValidate($id)
    {
        if(!$this->commonValidate()) {
            return false;
        }

        if(!$this->baseUpdateValidate($id)) {
            return false;
        }

        if(!$this->model($id)) {
            $this->error = 'not fund';
            return false;
        }

        return $this->processUpdateValidate($id);
    }

    //删除验证
    protected function destroyValidate($id)
    {
        if(!$this->baseDestroyValidate($id)) {
            return false;
        }

        if(!$this->model($id)) {
            return $this->notFond();
        }

        return $this->processDestroyValidate($id);
    }

    //删除验证
    protected function destroyBatchValidate()
    {
        if(!$this->baseDestroyBatchValidate()) {
            return false;
        }

        return $this->processDestroyBatchValidate();
    }

    //获取store保存的数据
    protected function getStoreFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        $data1 = $this->appendCommonFillData();
        $data2 = $this->appendStoreFillData();
        $data3 = [];

        if($this->appendCreatorAndUpdater) {
            $data3['creator'] = $this->loginUser->name?$this->loginUser->name:$this->loginUser->account;
        }

        if ($this->appendUserId) {
            $data3['user_id'] = $this->loginUser->id;
        }

        return array_merge($data,$data1,$data2,$data3);
    }

    //获取update保存的数据
    protected function getUpdateFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        $data1 = $this->appendCommonFillData();
        $data2 = $this->appendUpdateFillData();
        $data3 = [];

        if($this->appendCreatorAndUpdater) {
            $data3['updater'] = $this->loginUser->name?$this->loginUser->name:$this->loginUser->account;
        }

        return array_merge($data,$data1,$data2,$data3);
    }

    //新建时、更新时填充的额外数据
    protected function appendCommonFillData()
    {
        return [];
    }

    //新建时填充的额外数据
    protected function appendStoreFillData()
    {
        return [];
    }

    //更新时填充的额外数据
    protected function appendUpdateFillData()
    {
        return [];
    }

    //存储之后的操作
    protected function afterStore($model)
    {
        return true;
    }

    //更新之后的操作
    protected function afterUpdate($model)
    {
        return true;
    }

    //删除之后的操作
    protected function afterDestroy($model)
    {
        return true;
    }

    //存储之后返回前端的数据
    protected function getStoreRespondData($model)
    {
        return $model;
    }

    //显示详情返回前端的数据
    protected function getShowRespondData($model)
    {
        return $model;
    }

    //更新之后返回前端的数据
    protected function getUpdateRespondData($model)
    {
        return $model;
    }

    //删除数据
    protected function delete($model)
    {
        return $model->delete();
    }

    //去除null为空
    public function removeNullFromArray($data)
    {
        return array_map(function ($item){
            return $item === null ? '':$item;
        },$data);
    }

    //通用验证方法
    protected function requestValidate($rules,$messages=[])
    {
        $validator = Validator::make($this->request->all(),$rules,$messages);
        if ($validator->fails()) {
            $this->error = $validator->errors()->first();
            return false;
        }
        return true;
    }

}
