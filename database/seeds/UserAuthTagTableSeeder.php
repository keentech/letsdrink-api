<?php

use Illuminate\Database\Seeder;

class UserAuthTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            1=>['name' => '葡萄酒大师','code' => 'wineMaster'],
            2=>['name' => '葡萄酒评论家','code' => 'wineCritic'],
            3=>['name' => '葡萄酒媒体','code' => 'wineMedia'],
            4=>['name' => '葡萄酒讲师','code' => 'wineInstructor'],
            5=>['name' => '葡萄酒编辑','code' => 'wineEdition'],
            6=>['name' => '侍酒师','code' => 'sommelier'],
        ];

        \Illuminate\Support\Facades\DB::select('truncate table t_user_auth_tags');

        foreach ($data as $k=>$v){
            $m = new \App\Model\UserAuthTag();
            $m->id = $k;
            $m->fill($v);
            $m->save();
        }
    }
}
