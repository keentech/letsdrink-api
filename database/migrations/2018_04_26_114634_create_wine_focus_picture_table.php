<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWineFocusPictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_focus_picture', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',20)->comment('文案');
            $table->string('pic')->comment('封面图片');
            $table->string('obj_id')->comment('对应类型的id');
            $table->integer('weight')->comment('权重');
            $table->string('type')->comment('类型:article文章|product葡萄酒');
            $table->integer('user_id')->comment('创建人id');
            $table->string('creator')->comment('创建人');
            $table->string('updater')->comment('更新人');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_focus_picture');
    }
}
