<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class FetchWeixinArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:articles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch weixin articles';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Begin processing\n");

        $root = "/www/data";

        chdir($root);
        foreach (glob("*.json") as $item)
        {
            $this->processList($root."/".$item);
            //$this->updateCovers($root."/".$item);
        }

        $this->info("all is done");
    }

    protected function getUrlExt($url)
    {
        if (preg_match("/wx_fmt=([a-zA-Z0-9]+)/", $url, $matches))
        {
            return $matches[1];
        }
        else
        {
            return false;
        }
    }

    protected function saveQiNiuUrl($url)
    {
        $ext = '';

        if (preg_match("/wx_fmt=([a-zA-Z0-9]+)/", $url, $matches))
        {
            $ext = $matches[1];
        }
        
        $components = explode("/", $url);
        if (count($components) >= 5)
        {
            $path = 'letsDrink/image/weixin/'.$components[4];
            
            if ($ext) $path .= ".".$ext;

            $disk = Storage::disk('qiniu');

            $retry = 3;
            while (--$retry >= 0)
            {
                try 
                {
                    if ($disk->put($path, @file_get_contents($url)))
                    {
                        break;
                    }
                } 
                catch(Exception $ex) 
                {
                    $this->info("retry ".$url);
                }
            }

            return $path;
        }

        return false;
    }

    public function updateCovers($path)
    {
        $content = file_get_contents($path);
        $json = json_decode($content, true);

        if ($json['general_msg_list']) {
            $list = json_decode($json['general_msg_list'], true);

            foreach ($list['list'] as $item)
            {
                if (isset($item['app_msg_ext_info']))
                {

                    $article = \App\Model\Article::where('weixin_id', $item['app_msg_ext_info']['fileid'])->first();

                    if ($article && $item['app_msg_ext_info']['cover'])
                    {
                        $this->info("saving cover #".$article->id);
                        $path = $this->saveQiNiuUrl($item['app_msg_ext_info']['cover']);
                        if ($path !== false)
                        {
                            $ext = $this->getUrlExt($item['app_msg_ext_info']['cover']);
                            \App\Model\Article::saveCovers($article->id, [$path], "article_cover", "image/".$ext);
                        }

                        $article->save();
                        
                    }
                    
                    $this->info("done");
                }
            }

        }
        
    }

    public function processList($path)
    {
        $content = file_get_contents($path);
        $json = json_decode($content, true);

        if ($json['general_msg_list']) {
            $list = json_decode($json['general_msg_list'], true);

            foreach ($list['list'] as $item)
            {
                if (isset($item['app_msg_ext_info']))
                {
                    $ts = false;
                    if (isset($item['comm_msg_info']['datetime']))
                    {
                        $ts = date('Y-m-d H:i:s', $item['comm_msg_info']['datetime']);
                    }

                    $article = \App\Model\Article::where('weixin_id', $item['app_msg_ext_info']['fileid'])->first();
                    if ($article) {
                        $this->info("weixinId ".$item['app_msg_ext_info']['fileid']." exists, ignore\n");
                        continue;
                    }


                    $content_url = $item['app_msg_ext_info']['content_url'];

                    $this->info("fetch ".$content_url." ...");
                    $content = file_get_contents($content_url);
                    $this->info("done\n");
                    
                    $this->info("processing ...");

                    if (preg_match('/<div\s+[^>]*id="js_content">([\s\S]*?)<\/div>/', $content, $matches))
                    {
                        $content = preg_replace_callback('/<img(\s+[^>]*data-src="([^"]*)".*?)\/>/', function($out) {
                            $path = $this->saveQiNiuUrl($out[2]);
                            if ($path !== false)
                            {
                                $qiNiuUrl = env('QINIU_DOMAINS_DEFAULT').'/'.$path;

                                //$this->info("image ".$out[2]." saved into ".$qiNiuUrl);
                            
                                return '<img '.$out[1].' src="'.$qiNiuUrl.'"/>';
                            }
                            else
                            {
                                return $out[0];
                            }

                        }, $matches[1]);

                        $article = new \App\Model\Article();
                        $article->column_id = 3;
                        $article->type = 'article';
                        $article->online = 'yes';
                        $article->state = "publish";
                        $article->content = $content;
                        $article->abstract = $item['app_msg_ext_info']['digest'];
                        $article->author = $item['app_msg_ext_info']['author'];
                        $article->title = $item['app_msg_ext_info']['title'];
                        $article->weight = 10;
                        $article->weixin_id = $item['app_msg_ext_info']['fileid'];
                        if ($ts !== false)
                        {
                            $article->created_at = $ts;
                        }

                        $article->save();

                        if ($article->id && $item['app_msg_ext_info']['cover'])
                        {
                            $this->info("saving cover #".$article->id);
                            $path = $this->saveQiNiuUrl($item['app_msg_ext_info']['cover']);
                            if ($path !== false)
                            {
                                $ext = $this->getUrlExt($item['app_msg_ext_info']['cover']);
                                \App\Model\Article::saveCovers($article->id, [$path], "article_cover", "image/".$ext);
                            }

                            $article->save();
                        }

                        $this->info("article #".$article->id." saved");
                    }
                    
                    $this->info("done");

                    sleep(5);
                }
            }

        }

    }
}
