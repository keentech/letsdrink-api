<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class WineQrcodeRecordRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineQrcodeRecord";

    protected function relationLoad()
    {
        return ['product'];
    }

    protected function getShowRespondData($model)
    {
        return $model->load(['product','product.chateau']);
    }
}