<?php

namespace App\Model;

use App\BaseModel;

class UserRole extends BaseModel
{
    protected $table = 't_user_role';
}
