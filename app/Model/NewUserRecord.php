<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewUserRecord extends Model
{
    protected $table = "sys_new_user_records";
}
