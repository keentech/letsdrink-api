<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use App\Model\ArticleResource;
use App\Model\Resource;
use App\Model\ResourceTable;
use Illuminate\Support\Facades\DB;

class DailySignRepository extends BaseRepository
{
    protected $modelName = "App\Model\Article";

    protected function query()
    {
        return $this->model()->where('column_id','=',1);
    }

    protected function beforeListShow($model)
    {
        return $model->orderBy('online_at','desc')->orderBy('updated_at','desc');
    }

    //列表页load其他关联数据
    protected function relationLoad()
    {
        return ['column'];
    }

    //格式化show显示的数据
    protected function getShowRespondData($model)
    {
        return $model->load('column','covers');
    }

    protected function commonRules()
    {
        return [
            'title' => 'required',
            'cover_ids' => 'required',
            'online_at' => 'required|date',
            //'state' => 'required|in:publish'
        ];
    }

    protected function commonSave($model)
    {
        return $this->saveCovers($model);
    }

    //保存封面
    private function saveCovers($model)
    {
        if ($this->request->exists('cover_ids')) {
            $ids = $this->request->input('cover_ids');

            $rows = $ids?explode(',',$ids) : [];

            $db = DB::table('sys_resource_table')->where('obj_id','=',$model->id)
                ->where('obj_type','=','article_cover');

            $o_ids = $db->pluck('resource_id')->toArray();

            $db->whereIn('resource_id',array_diff($o_ids,$rows))->delete();

            $rows = array_diff($rows,$o_ids);

            foreach ($rows as $row) {
                $AR = new ResourceTable();
                $AR->obj_id = $model->id;
                $AR->resource_id = $row;
                $AR->user_id = $this->loginUser->id;
                $AR->obj_type = 'article_cover';
                $AR->save();
            }
            return true;
        }
        return false;
    }


    //获取保存到数据库的数据
    protected function getStoreFillData()
    {
        $data = $this->removeNullFromArray($this->request->all());
        return array_merge($data,['user_id'=>$this->loginUser->id,'column_id'=>1,'state'=>'publish']);
    }

    protected function processStoreValidate()
    {
        return $this->validateOnlineAt();
    }

    protected function processUpdateValidate($id)
    {
        return $this->validateOnlineAt($id);
    }

    private function validateOnlineAt($id=false)
    {
        $online_at =  date('Y-m-d',strtotime($this->request->input('online_at')));
        $start = $online_at.' 00:00:00';
        $end = $online_at.' 23:59:59';

        $c = DB::table('cms_article')->whereRaw(($id?'`id`<>'.$id.' and ':' ').'`online_at`>="'.$start .'" and `online_at` <="'.$end.'" and deleted_at is null' )
            ->where('column_id','=',1)
            ->count();

        if ($c > 0 ){
            $this->error = '该日期的每日签已存在';
            return false;
        }
        return true;
    }
}
