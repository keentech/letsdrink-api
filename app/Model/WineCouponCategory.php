<?php

namespace App\Model;

use App\BaseModel;
use Illuminate\Support\Facades\DB;

class WineCouponCategory extends BaseModel
{
    protected $table = "wine_coupon_categories";

    protected $fillable = [
        'category',
        'importer_id',
        'dealer_id',
        'loseType',
        'full_amount',
        'amount',
        'product_id',
        'user_id',
        'updater',
        'creator',
    ];

    protected $appends = ['importerName','productName','dealerName','loseTypeName','couponName'];

    public function importer()
    {
        return $this->belongsTo('App\Model\Importer','importer_id','id');
    }

    public function dealer()
    {
        return $this->belongsTo('App\Model\Dealer','dealer_id','id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\WineProduct','product_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function getCouponNameAttribute()
    {
        switch ($this->category)
        {
            case "general":
                $name = "全场";
                break;
            case "product":
                $name = $this->productName;
                break;
            case "importer":
                $name = $this->importerName;
                break;
            case "dealer":
                $name = $this->dealerName;
                break;
            default:
                $name = '';
                break;
        }
        return $name.'-'.$this->loseTypeName;
    }

    public function getCategoryNameAttribute()
    {
        $arr = ["general" =>"全场", "product" => "指定商品","importer" =>"指定进口商", "dealer" => "指定经销商"];
        return isset($arr[$this->category])?$arr[$this->category]:$this->category;
    }

    public function getProductNameAttribute()
    {
        $s = DB::table('wine_product')->where('id','=',$this->product_id)->select('name','chname')->first();
        return $s?$s->chname:'';
    }
    public function getImporterNameAttribute()
    {
        $s = DB::table('sys_importers')->where('id','=',$this->importer_id)->select('name')->first();
        return $s?$s->name:'';
    }

    public function getDealerNameAttribute()
    {
        $s = DB::table('sys_dealers')->where('id','=',$this->dealer_id)->select('name')->first();
        return $s?$s->name:'';
    }

    public function getLoseTypeNameAttribute()
    {
        if ($this->loseType == "direct") {
            return "直减".floatval($this->amount);
        }
        if ($this->loseType == "full"){
            return "满".floatval($this->full_amount).'减'.floatval($this->amount);
        }
        return '';
    }
}
