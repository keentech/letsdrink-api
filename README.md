# 部署说明
    1.有赞包出现curl: (60) SSL certificate problem问题则需要修改
        vendor/hanson\foundation-sdk\src/Http.php 42行 
        添加：CURLOPT_SSL_VERIFYPEER => false,
        
    结果如下：
        protected static $defaults = [
            'curl' => [
                CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
                CURLOPT_SSL_VERIFYPEER => false,
            ],
        ];
    2.有赞包修改
        vendor/hanson\youzan-sdk\src/Api.php 47行如下：
        return $result;

        //if (isset($result['error_response'])) {
        //    throw new YouzanException($result['error_response']['msg'], $result['error_response']['code']);
        //}

        //return $result['response'];
    
# Drink API
   api/admin/
   
   1.后台resetful接口（api/admin/restful?_model=下列参数）
        
        resource                    //上传的文件资源

        user                        //用户
        expert-user                 //专家

        cms-article                 //文章
        cms-daily-sign              //每日签
        cms-article-column          //文章栏目

        wine-chateau                //酒庄
        wine-chateau-grapery        //酒庄葡萄
        wine-product                //葡萄酒
        wine-product-drink          //葡萄酒Drink介绍
        wine-product-judge          //葡萄酒专业品鉴
        wine-product-technique      //葡萄酒技术信息
        wine-product-score          //葡萄酒专业评分

        wine-scene                  //消费场景

        sys-admin                   //管理员
        sys-country                 //国家
        
        wine-product-qrcode         //产品二维码发放
        wine-qrcode-record          //二维码记录
        
        3.api/admin/destroyBatch?_model=
            说明：批量删除    
            方式：POST
            参数：
              ids      记录id：逗号隔开
        
   2.后台非restful接口（api/admin/下列参数）
   
        1.file-upload     
            说明：文件上传    
            方式：POST
            参数：
            file         单文件
            uploadType   上传方式(默认qiniu)：loacl本地|qiniu七牛
        
        2.state-change               
            说明：状态切换，上线下线 权重修改    
            方式：POST
            参数：
            changeType      修改的类型：article文章|chateau酒庄|product葡萄酒
            changeKey       修改的键：  online上下线|weight权重
            changeValue     修改的值：  online=>yes上线|no下线，weight=>数字
            changeId        修改的记录id
           
        3.qrcode-{id}-export
            说明：到处二维码编码    
            方式：get
            参数：
            id      发放的二维码记录id
            
## 20180702
    
    ALTER TABLE wine_product CHANGE `sell_method` `sell_method` ENUM('online','offline','show') NOT NULL COMMENT '售卖方式';
    ALTER TABLE wine_product_expert_judge CHANGE `short_review` `short_review` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '一句话点评';
    ALTER TABLE wine_product_cuisine_relations CHANGE `text` `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
    
## 20180801
   ALTER TABLE wine_coupons CHANGE `can_get_start` `can_get_start` DATE DEFAULT NULL COMMENT '领取有效期开始';
   ALTER TABLE wine_coupons CHANGE `can_get_end` `can_get_end` DATE DEFAULT NULL COMMENT '领取有效期结束';
   ALTER TABLE wine_coupons CHANGE `can_use_start` `can_use_start` DATE DEFAULT NULL COMMENT '使用有效期开始';
   ALTER TABLE wine_coupons CHANGE `can_use_end` `can_use_end` DATE DEFAULT NULL COMMENT '使用有效期结束';
   ALTER TABLE t_user_coupons CHANGE `expired_at` `expired_at` DATE DEFAULT NULL COMMENT '过期时间';
   INSERT INTO `sys_seting_configs`(id,label,`key`,`value`,show_frontend) VALUE(3,'每日红包','day_user_coupon','',0);