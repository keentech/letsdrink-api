<?php

namespace App\Http\Middleware;

use App\Helpers\ApiResponseTrait;
use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;

class adminJWT
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            JWTAuth::parseToken()->authenticate();
            if (!in_array(auth()->user()->userType,['admin','super'])){
                return $this->failed('no power!', 401);
            }
        }catch (\Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return $this->failed('Token 无效', 401);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return $this->failed('Token 已过期', 401);
            } else {
                return $this->failed($e->getMessage(), 401);
            }
        }
        return $next($request);
    }
}
