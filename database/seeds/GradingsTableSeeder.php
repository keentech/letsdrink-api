<?php

use Illuminate\Database\Seeder;

class GradingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //产品档次
        $items = [
            ['id' =>1 ,'name' => '收藏级'],
            ['id' =>2 ,'name' => '精品级'],
            ['id' =>3 ,'name' => '随意饮'],
        ];

        \Illuminate\Support\Facades\DB::table('wine_gradings')->insert($items);
    }
}
