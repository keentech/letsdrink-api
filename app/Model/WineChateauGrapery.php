<?php

namespace App\Model;

use App\BaseModel;

class WineChateauGrapery extends BaseModel
{
    protected $table = "wine_chateau_grapery";

    protected $fillable = [
        'chateau_id',
        'name',
        'proportion',
        'user_id',
    ];

    public function chateau()
    {
        return $this->belongsTo('App\Model\WineChateau','chateau_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
