<?php

namespace App\Model;

use App\BaseModel;

class ImporterDealerRelation extends BaseModel
{
    protected $table = "sys_importer_dealer_relation";

    protected $fillable = [
        'importer_id',
        'dealer_id',
        'remark',
        'user_id',
        'updater',
        'creator',
    ];

    public function importer()
    {
        return $this->belongsTo('App\Model\Importer','importer_id','id');
    }

    public function dealer()
    {
        return $this->belongsTo('App\Model\Dealer','dealer_id','id');
    }
}
