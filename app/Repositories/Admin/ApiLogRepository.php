<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Support\Facades\DB;

class ApiLogRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\ApiLog";

    protected function query()
    {
        return DB::table('api_log');
    }

    public function beforeListShow($model)
    {
        return $model->orderBy('createTime','desc');
    }

    protected $allowApiMethod = ['index','show'];
}