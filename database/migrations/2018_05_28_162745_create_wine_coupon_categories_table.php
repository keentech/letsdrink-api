<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWineCouponCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_coupon_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category')->comment('优惠券类型:普通general|指定商品product|指定进口商importer|指定经销商dealer');
            $table->integer('importer_id')->default(0)->comment('进口商');
            $table->integer('dealer_id')->default(0)->comment('经销商');
            $table->string('loseType')->comment('满减类型:direct直减|full满减');
            $table->decimal('full_amount',12,2)->default(0)->comment('满金额');
            $table->decimal('amount',12,2)->nullable()->comment('减掉的金额');
            $table->integer('product_id')->default(0)->comment('商品id');
            $table->integer('user_id');
            $table->string('updater');
            $table->string('creator');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_coupon_categories');
    }
}
