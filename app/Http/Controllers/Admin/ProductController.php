<?php

namespace App\Http\Controllers\Admin;

use App\Model\WineProductScene;
use Illuminate\Support\Facades\DB;

class ProductController extends BaseController
{
    protected function category()
    {
        $items = DB::table('wine_product')->whereRaw('category is not null group by category')
            ->select('category')->pluck('category');
        return $this->success($items);
    }

    public function sceneTree()
    {
        return WineProductScene::tree();
    }
}
