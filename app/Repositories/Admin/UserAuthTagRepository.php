<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class UserAuthTagRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\UserAuthTag";

    protected $allowApiMethod = ['index','show'];

    //显示
    public function show($id)
    {
        if(!$this->checkAuth('show')) {
            return $this->error;
        }

        if($model = $this->model($id)){

            $respond = $this->getShowRespondData($model);

            $this->saveShowLog($id);

            return $this->success($respond);
        }

        if($model = $this->query()->where('code','=',$id)->first()){

            $respond = $this->getShowRespondData($model);

            $this->saveShowLog($id);

            return $this->success($respond);
        }

        return $this->notFond();
    }
}