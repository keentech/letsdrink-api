<?php

namespace App\Model;

use App\BaseModel;

class WineProductQrcode extends BaseModel
{
    protected $table = "wine_product_qrcode";

    protected $appends = ['download_url'];

    //关联酒庄
    public function chateau()
    {
        return $this->belongsTo('App\Model\WineChateau','chateau_id','id');
    }
    //关联产品
    public function product()
    {
        return $this->belongsTo('App\Model\WineProduct','product_id','id');
    }
    //关联生产商
    public function produce()
    {
        return $this->belongsTo('App\Model\SysProduce','produce_id','id');
    }
    //关联用户
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    //关联二维码库
    public function qrcodeRecord()
    {
        return $this->hasMany('App\Model\WineQrcodeRecord','qrcode_id','id');
    }
    //下载地址
    public function getDownloadUrlAttribute()
    {
        return asset('api/admin/qrcode-'.$this->id.'-export');
    }
    //更新剩余可用数量
    public function updateRemainAmount()
    {
        $this->remainAmount = $this->qrcodeRecord()->where('state','=',0)->count();
        $this->save();
    }

}
