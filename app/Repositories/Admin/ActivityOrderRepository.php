<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class ActivityOrderRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\ActivityOrders";

    protected $allowApiMethod = ['index','show','destroy','destroyBatch'];

    protected function beforeListShow($model)
    {
        return $model->orderBy('activity_id','desc')->orderBy('apply_pass_time','asc')->orderBy('id','asc');
    }

}