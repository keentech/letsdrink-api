<?php

namespace App\Http\Controllers\Admin;

use App\Model\Activity;
use App\Model\ActivityOrders;
use App\Model\WeChatNotify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ActivityController extends BaseController
{
    //通过
    public function changeState(Request $request,$id)
    {
        if(!$this->requestValidate($request->all(),['state'=>'required|in:pass,unpass'])){
            return $this->failed($this->error);
        }

        $ao = ActivityOrders::where('apply_state','=',1)->find($id);

        if (!$ao) return $this->notFond();

        $activity = $ao->activity;

        if ($activity->sign_count>=$activity->participate_limit){
            //更改其它报名记录
            ActivityOrders::changeState($activity);
            return $this->failed('报名人数已满');
        }

        $ao->state = $request->input('state')=="pass"?3:0;
        $ao->save();

        //更新报名通过人数

        $c = DB::table('activity_orders')->where('activity_id','=',$activity->id)
            ->whereNull('deleted_at')
            ->whereIn('state',[3,4,5,7])
            ->count();

        if ($activity->deadline < date('Y-m-d') || $activity->participate_limit<=$c){
            $activity->state = "finished";
        }

        $activity->sign_count = $c;
        $activity->save();

        //更改其它报名记录
        ActivityOrders::changeState($activity);

        if ($ao->state == 3 && $ao->send_pass==0) {
            $m = new WeChatNotify();
            if($m->applyPass($ao)){
                $ao->send_pass =1;
                $ao->send_pass_time =  date('Y-m-d H:i:s');
                $ao->save();
            }
        }


        return $this->message('ok');
    }
    //发货
    public function delivery(Request $request)
    {
        $rules = [
            'id' => [
                'required',
                Rule::exists('activity_orders', 'id')->where('is_delivery','no')
            ],
            'express_name' => 'required',
            'express_number' => 'required'
        ];

        $messages = [
            'id.exists' => '商品已发货，请勿重复',
            'express_number.required' => "物流单号必填",
            'express_name.required' => "物流公司必选"
        ];

        if (!$this->requestValidate($request->all(),$rules,$messages)){
            return $this->failed($this->error);
        }

        $data = $request->only('express_name','express_number');

        $ao = ActivityOrders::find($request->input('id'));
        $ao->fill($data);
        $ao->delivery_time = date('Y-m-d H:i:s');
        $ao->is_delivery = 'yes';
        $ao->state = 4;
        $ao->save();

        $m = new WeChatNotify();
        $m->activityOrderDelivery($ao);

        return $this->message('ok');

    }

    public function getNextSimpleList($id)
    {
        $at = Activity::find($id);
        $items = DB::table('activities')->where('state','=','publish')->where('start_time','>=',$at->start_time)
            ->where('id','<>',$id)
            ->select('id','name')
            ->get();

        return $this->success($items);
    }

    public function setNext($id,$nextId)
    {
        $at = Activity::find($id);
        $at->next_id = $nextId;
        $at->save();

        return $this->message('ok');
    }
}
