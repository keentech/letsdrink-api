<?php

namespace App\Model;

use App\BaseModel;
//产品口味定义
class WineTaste extends BaseModel
{
    protected $table = "wine_tastes";

    protected $fillable = [
        'name',
        'icon'
    ];

    protected $visible = ["id",'name',"icon"];

}
