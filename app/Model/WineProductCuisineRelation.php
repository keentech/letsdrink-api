<?php

namespace App\Model;

use App\BaseModel;

class WineProductCuisineRelation extends BaseModel
{
    protected $table = "wine_product_cuisine_relations";

    protected $fillable = [
        'wine_cuisines_id',
        'name',
        'icon',
        'text',
    ];

    protected $visible = ["id","product_id","wine_cuisines_id",'name',"icon","text"];

}
