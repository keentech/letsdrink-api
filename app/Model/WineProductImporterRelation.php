<?php

namespace App\Model;

use App\BaseModel;

class WineProductImporterRelation extends BaseModel
{
    protected $table = "wine_product_importer_relations";

}
