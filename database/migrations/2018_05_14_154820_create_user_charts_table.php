<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_user_charts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->comment('日期');
            $table->integer('total')->default(0)->comment('新增用户总数量');
            $table->integer('order_total')->default(0)->comment('订单新增用户总数量');
            $table->integer('view_total')->default(0)->comment('浏览新增用户总数量');
            $table->integer('view_qr_total')->default(0)->comment('浏览二维码来源新增');
            $table->integer('view_other_total')->default(0)->comment('浏览其他来源新增');
            $table->integer('order_qr_total')->default(0)->comment('订单二维码来源新增');
            $table->integer('order_other_total')->default(0)->comment('订单其他来源新增');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_charts');
    }
}
