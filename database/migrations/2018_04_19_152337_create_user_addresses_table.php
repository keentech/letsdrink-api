<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->comment('用户id');
            $table->integer('default')->default(0);
            $table->string('userName')->comment('收货人姓名');
            $table->string('postalCode')->comment('邮编');
            $table->string('provinceName')->comment('国标收货地址第一级地址');
            $table->string('cityName')->comment('国标收货地址第二级地址');
            $table->string('countyName')->comment('国标收货地址第三级地址');
            $table->string('detailInfo')->comment('详细收货地址信息');
            $table->string('nationalCode')->comment('收货地址国家码');
            $table->string('telNumber')->comment('收货人手机号码');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_addresses');
    }
}
