<?php

namespace App\Model;

use App\BaseModel;

class WineProductDeployment extends BaseModel
{
    protected $table = "wine_product_deployment";

    protected $fillable = [
        'product_id',
        'name',
        'scale',
        'user_id',
        'harvestDate'
    ];

    protected $visible = [
      'id','name','scale','harvestDate'
    ];
}
