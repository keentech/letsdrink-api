<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class CountryRepository extends BaseRepository
{

    //模型名称
    protected $modelName = "App\Model\SysCountry";

    protected $allowApiMethod = ['index',"store","show"];


    protected function beforeListShow($model)
    {
        return $model->orderBy('created_at','desc')->orderBy('id','desc');
    }

    protected function commonRules()
    {
        return [
            'country' => 'required',
            'national_flag' => 'required',
        ];
    }

    protected function commonMessages()
    {
        return [
            'country.required' => '国家名称必填',
            'national_flag.required' => '未上传国旗',
        ];
    }

    public function export()
    {
        $data = $this->query()->orderBy('id','asc')->select(['id','country','chCountry','countryCode'])
            ->get()->toArray();
        return $this->exportFile('国家码模板',$data);
    }
}