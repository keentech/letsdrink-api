<?php

use Illuminate\Database\Seeder;

class TasteKeywordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //drink味道关键词

        $items = \App\Helpers\Helpers::getTempJson('TasteKeywords',true);


        \Illuminate\Support\Facades\DB::select('truncate table wine_taste_keywords');
        \Illuminate\Support\Facades\DB::table('wine_taste_keywords')->insert($items);
    }
}
