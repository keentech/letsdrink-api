<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class ImporterDealerRelationRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\ImporterDealerRelation";

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    protected function relationLoad()
    {
        return ['dealer'];
    }

    protected function getShowRespondData($model)
    {
        return $model->load('importer','dealer');
    }

    protected function commonRules()
    {
        return [
            'importer_id'=>'required',
            'dealer_id'=>'required',
        ];
    }
}