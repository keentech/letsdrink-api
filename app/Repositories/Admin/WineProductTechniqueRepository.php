<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class WineProductTechniqueRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineProductTechnique";

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    protected function commonRules()
    {
        return [
            'product_id' => 'required',
        ];
    }

    protected function processStoreValidate()
    {
        $c = $this->query()->where('product_id','=',$this->request->input('product_id'))->count();
        if($c > 0) {
            $this->error = '记录已存在，请勿重复添加！';
            return false;
        }
        return true;
    }
}