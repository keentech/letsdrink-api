<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            //订单基础信息
            $table->string('out_trade_no')->comment('订单号')->nullable();
            $table->string('md5_out_trade_no')->comment('md5订单号,用于送礼分享key识别')->nullable();
            $table->integer('state')->comment('状态:0待支付|1待发货|2已取消|3未领取|4已过期|5已发货|6已完成|7交易关闭|8已领取待使用|9已过期退款中|10已过期已退款')->nullable();
            $table->string('origin')->comment('来源类型:general普通|qrcode二维码');
            $table->string('origin_qrcode')->comment('来源二维码')->nullable();
            $table->string('type')->default('general')->comment('类型:general普通|gift礼物');
            $table->dateTime('expire_time')->comment('过期时间')->nullable();
            $table->dateTime('finish_time')->comment('完成时间(签收时间)')->nullable();

            $table->decimal('deliver_fee',12,2)->default(0)->comment('运费');
            $table->decimal('origin_product_total_fee',12,2)->comment('商品原总金额');
            $table->decimal('product_total_fee',12,2)->comment('实际商品总金额');
            $table->decimal('total_fee',12,2)->comment('订单总金额(包含运费)');


            //产品信息
            $table->integer('product_id')->comment('葡萄酒id');
            $table->string('product_name')->comment('葡萄酒名称');
            $table->string('product_chname')->comment('葡萄酒中文名称');
            $table->decimal('origin_price',12,2)->comment('原价');
            $table->decimal('price',12,2)->comment('实际价格');
            $table->integer('quantity')->comment('商品数量');
            //$table->integer('count')->comment('数量');

            //收件方信息
            $table->integer('get_user_id')->comment('收件人id')->nullable();
            $table->string('username')->comment('收件人名称')->nullable();
            $table->string('tel_number')->comment('收件人手机')->nullable();
            $table->string('address')->comment('收件人地址')->nullable();
            $table->string('blessing')->comment('送礼人祝福')->nullable();
            $table->integer('address_id')->nullable()->comment('收件人地址id');
            $table->dateTime('get_time')->nullable()->comment('收件人领取时间');


            //订单创建人信息
            $table->integer('create_user_id')->comment('创建人id');
            $table->string('comment')->comment('买家留言')->nullable();

            //支付、物流信息
            $table->enum('is_pay',['no','yes'])->default('no')->comment('是否已支付');
            $table->dateTime('pay_time')->comment('支付时间')->nullable();
            $table->enum('is_delivery',['no','yes'])->default('no')->comment('是否已发货');
            $table->dateTime('delivery_time')->comment('是否已发货')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
