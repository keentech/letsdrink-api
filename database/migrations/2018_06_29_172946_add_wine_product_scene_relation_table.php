<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWineProductSceneRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wine_product_scene_relation', function (Blueprint $table) {
            $table->string('scene_one_id')->nullable()->comment('第一级');
            $table->string('scene_two_id')->nullable()->comment('第二级');
            $table->string('scene_three_id')->nullable()->comment('第三级');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wine_product_scene_relation', function (Blueprint $table) {
            //
        });
    }
}
