<?php

namespace App\Model;

use App\BaseModel;

class WineProductTechnique extends BaseModel
{
    protected $table = "wine_product_technique";

    protected $fillable = [
        'product_id',
        'user_id',
        'harvestDate',
        'makeMethod',
        'produceDate',
        'produceEndDate',
        'oakType',
        'yields',
        'makerId',
        'makerName',
        'makerPicture',
        'makerSummary',
        'makeStyle',
        'adviserId',
        'adviserName',
        'advisePicture',
        'adviserSummary',
        'remark',
        'updater',
        'creator',
        'supervisorName',
        'supervisorPicture',
        'supervisorSummary'
    ];
}
