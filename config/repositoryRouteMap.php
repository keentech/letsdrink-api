<?php
/*
 * 路由关联模型民称
 * key => 路由名称
 * value => 模型名称
 *
 */
return [
    //后台
    'admin' => [
        'resource' => 'Resource',

        'user' => 'User',                                    //用户
        'user-address' => 'UserAddress',                     //用户地址
        'user-location' => 'UserLocation',                     //用户地址
        'expert-user' => 'ExpertUser',                       //专家

        'cms-article' => 'Article',                          //文章
        'cms-daily-sign' => 'DailySign',                     //每日签
        'cms-article-column' => 'ArticleColumn',             //文章栏目

        'wine-chateau' => 'WineChateau',                     //酒庄
        'wine-chateau-grapery' => 'WineChateauGrapery',      //酒庄葡萄
        'wine-product' => 'WineProduct',                     //葡萄酒
        'wine-product-drink' => 'WineProductDrink',          //葡萄酒Drink介绍
        'wine-product-judge' => 'WineProductJudge',          //葡萄酒专业品鉴
        'wine-product-technique' => 'WineProductTechnique',  //葡萄酒技术信息
        'wine-product-score' => 'WineProductScore',          //葡萄酒专业评分

        'wine-product-qrcode' => 'WineProductQrcode',        //产品二维码发放
        'wine-qrcode-record'  => 'WineQrcodeRecord',         //二维码记录

        'wine-focus-picture' => 'WineFocusPicture',          //葡萄酒焦点图

        'wine-scene' =>'WineProductScene',                   //消费场景

        'order' => 'Order',                                  //订单
        'express' => 'Express',                              //订单物流
        'express-carrier' => 'ExpressCarrier',              //物流公司

        'sys-country' => 'Country',                          //国家
        'sys-subdivision' =>'SysSubdivision',                //地区
        'sys-g-region' =>'GRegion',                          //大区码
        'sys-region' =>'SysRegion',                          //厂区码
        'sys-produce' =>'SysProduce',                        //生产商

        'sys-importer' =>'Importer',                        //进口商
        'sys-dealer' => 'Dealer',                           //经销商
        'sys-importer-dealer' =>'ImporterDealerRelation',   //进口商经销商
        'sys-areas' =>'SysAreas',                           //区域信息

        'sys-coupon-category' => 'WineCouponCategory',     //优惠券类型
        'sys-coupon' => 'WineCoupon',                      //优惠券活动

        'use-coupon' => 'UserCoupon',                       //用户优惠券
        'sys-seting' => 'SysSetingConfig',                  //配置参数

        'sys-admin' => 'Admin',                              //管理员

        'sys-api-log' => 'ApiLog',                           //api日志

        'user-auth-tag' => 'UserAuthTag',                     //身份认证

        'wine_today_potable' => 'WineTodayPotables',           //今日适饮
        'wine-taste-keywords' => 'WineTasteKeywords',           //drink味道关键词
        'wine-taste' => 'WineTaste',                            //产品口味
        'wine-grading' => 'WineGrading',                        //产品档次
        'wine-cuisine' => 'WineCuisines',                      //菜系选择

        'activity' => 'Activity',                               //活动
        'activity-order' => 'ActivityOrder',           //免费活动订单

    ],
    //前台
    'home'  => []

];
?>