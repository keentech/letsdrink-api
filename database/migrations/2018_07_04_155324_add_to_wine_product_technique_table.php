<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToWineProductTechniqueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wine_product_technique', function (Blueprint $table) {
            $table->string('supervisorName')->nullable()->comment('葡萄园主管名称');
            $table->string('supervisorPicture')->nullable()->comment('葡萄园主管头像');
            $table->string('supervisorSummary')->nullable()->comment('葡萄园主管简介');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wine_product_technique', function (Blueprint $table) {
            //
        });
    }
}
