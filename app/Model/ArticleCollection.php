<?php

namespace App\Model;

use App\BaseModel;

class ArticleCollection extends BaseModel
{
    protected $table = "t_user_article_collections";

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function article()
    {
        return $this->belongsTo('App\Model\Article','article_id','id');
    }
}
