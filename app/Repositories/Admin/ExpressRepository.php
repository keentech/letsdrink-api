<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class ExpressRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\Express";
}