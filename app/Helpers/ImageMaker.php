<?php

namespace App\Helpers;

use Intervention\Image\ImageManager;

class ImageMaker
{
    public function setFileDir()
    {
        @mkdir(storage_path('app/public/canvas'));
        return storage_path('app/public/canvas');
    }

    public function setFilename()
    {
        return md5(time().str_random(32)).'.jpg';
    }

    public function setSize($w=400,$h=400)
    {
        $this->w = $w;
        $this->h = $h;
    }

    public function create($data)
    {
        /* Set width and height in proportion of genuine PHP logo */
        $width = 400;
        $height = 400;

        /* Create an Imagick object with transparent canvas */
        $img = new \Imagick();
        $img->newImage($width,$height,'black','png');

        $draw = new \ImagickDraw();
        $draw->setFillColor('#CCCCCC');
        $draw->setStrokeColor('#555555');
        $draw->setStrokeWidth(5);
        $draw->arc(100,100,50,50,10,90);

        $img->drawImage($draw);

        header('Content-Type: image/png');
        echo $img->getImageBlob();


    }

    public function create1($data)
    {
        $obj = new ImageManager(['driver' => 'imagick']);
        $obj = $obj->canvas(400,400,'#FFF');

        $obj->circle(160,100,100,function ($draw){
            $draw->border(20, '#b01344');
        });

        return $obj;
    }

    public static function make($data)
    {
        $w = 800;
        $h = 900;

        $d_1 = intval($data[0] *360);
        $d_2 = intval($data[1] *360);
        $d_3 = intval($data[2] *360);
        $d_4 = intval($data[3] *360);

        $b_w = 100;
        $h_w = 50;
        $g_h = 0;


        $path = storage_path('app/public/');
        $file = 'canvas/'.md5(time().str_random(32)).'.jpg';
        $realPath = $path.$file;

        // 创建图像
        $image = imagecreatetruecolor($w, $h);
        imagesavealpha($image, true);
        $trans_colour = imagecolorallocatealpha($image, 0, 0, 0, 127);
        imagefill($image, 0, 0, $trans_colour);

        // 分配一些颜色
        $white      = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
        $hc_color   = imagecolorallocate($image, 0xC0, 0xC0, 0xC0);
        $h_color    = imagecolorallocate($image, 174, 19, 68);
        $font_color = imagecolorallocate($image, 0, 0, 0);
        $font_size  = 30;
        $w_1_4 = 1/4 * $w;
        $w_1_2 = 1/2 * $w;

        //第一个圆（左上）
        imagefilledarc($image, $w_1_4, $w_1_4, $w_1_2 - $b_w , $w_1_2 - $b_w , 270, 270+$d_1 , $h_color, IMG_ARC_PIE);
        imagefilledarc($image, $w_1_4, $w_1_4, $w_1_2 - $b_w , $w_1_2 - $b_w , 270+$d_1, 270 , $hc_color, IMG_ARC_PIE);
        imagefilledellipse($image, $w_1_4, $w_1_4, $w_1_2-$b_w-$h_w, $w_1_2-$b_w-$h_w, $white);
        imagefilledellipse($image,$w_1_4, 0.5*$b_w+$h_w*0.25,  $h_w*0.5,$h_w*0.5, $h_color);
        imagettftext($image,$font_size,0,$w_1_4-1.25*$font_size,$w_1_4+0.25*$h_w,$font_color,storage_path('app/simkai.ttf'),'酸度');

        //第二个圆（右上）
        imagefilledarc($image, $w_1_4+$w_1_2, $w_1_4, $w_1_2 - $b_w , $w_1_2 - $b_w , 270, 270+$d_2 , $h_color, IMG_ARC_PIE);
        imagefilledarc($image, $w_1_4+$w_1_2, $w_1_4, $w_1_2 - $b_w , $w_1_2 - $b_w , 270+$d_2, 270 , $hc_color, IMG_ARC_PIE);
        imagefilledellipse($image, $w_1_4+$w_1_2, $w_1_4, $w_1_2-$b_w-$h_w, $w_1_2-$b_w-$h_w, $white);
        imagefilledellipse($image,$w_1_4+$w_1_2, 0.5*$b_w+$h_w*0.25,  $h_w*0.5,$h_w*0.5, $h_color);
        imagettftext($image,$font_size,0,$w_1_2+$w_1_4-1.25*$font_size,$w_1_4+0.25*$h_w,$font_color,storage_path('app/simkai.ttf'),'甜度');

        //第三个圆（左下）
        imagefilledarc($image, $w_1_4, $w_1_4+$w_1_2+$g_h, $w_1_2 - $b_w , $w_1_2 - $b_w , 270, 270+$d_4 , $h_color, IMG_ARC_PIE);
        imagefilledarc($image, $w_1_4, $w_1_4+$w_1_2+$g_h, $w_1_2 - $b_w , $w_1_2 - $b_w , 270+$d_4, 270 , $hc_color, IMG_ARC_PIE);
        imagefilledellipse($image, $w_1_4, $w_1_4+$w_1_2+$g_h, $w_1_2-$b_w-$h_w, $w_1_2-$b_w-$h_w, $white);
        imagefilledellipse($image,$w_1_4, 0.5*$b_w+$h_w*0.25+$w_1_2+$g_h,  $h_w*0.5,$h_w*0.5, $h_color);
        imagettftext($image,$font_size,0,$w_1_4-1.25*$font_size,$w_1_2+$w_1_4+0.25*$h_w,$font_color,storage_path('app/simkai.ttf'),'瑟度');


        //第四个圆（右下）
        imagefilledarc($image, $w_1_4+$w_1_2, $w_1_4+$w_1_2+$g_h, $w_1_2 - $b_w , $w_1_2 - $b_w , 270, 270+$d_3 , $h_color, IMG_ARC_PIE);
        imagefilledarc($image, $w_1_4+$w_1_2, $w_1_4+$w_1_2+$g_h, $w_1_2 - $b_w , $w_1_2 - $b_w , 270+$d_3, 270 , $hc_color, IMG_ARC_PIE);
        imagefilledellipse($image, $w_1_4+$w_1_2, $w_1_4+$w_1_2+$g_h, $w_1_2-$b_w-$h_w, $w_1_2-$b_w-$h_w, $white);
        imagefilledellipse($image,$w_1_4+$w_1_2, 0.5*$b_w+$h_w*0.25+$w_1_2+$g_h,  $h_w*0.5,$h_w*0.5, $h_color);
        imagettftext($image,$font_size,0,$w_1_2+$w_1_4-1.75*$font_size,$w_1_2+$w_1_4+0.25*$h_w,$font_color,storage_path('app/simkai.ttf'),'酒精度');



        // 输出图像
        header('Content-type: image/png');
        imagepng($image);
        imagedestroy($image);

    }
}
