<?php

namespace App\Helpers;

use Hanson\Youzan\Youzan;

trait YouZanTrait
{
    protected $YouZan;

    //请求获取响应
    protected function you_zan_respond($method, $params = [], $files = [], $version = '3.0.0')
    {
        $this->YouZan = $youzan = new Youzan(\Config::get('youzan'));

        $resp = $youzan->request($method, $params, $files, $version);

        //记录日志
        $log  = 'method:'.$method;
        $log .= '   params:'.json_encode($params);
        $log .= '   respond:'.json_encode($resp);
        Helpers::log($log,'youzan_respond');

        return $resp;
    }

    //获取店铺信息
    public function getShop()
    {
        return $this->you_zan_respond('youzan.shop.get');
    }

    //获取商品详细
    public function getItem($id)
    {
        return $this->you_zan_respond('youzan.item.get',['item_id'=>$id]);
    }

    //获取出售中商品列表
    public function getItemsOnSale()
    {
        return $this->you_zan_respond('youzan.items.onsale.get');
    }

    //获取仓库中商品列表
    public function getItemsInventory()
    {
        return $this->you_zan_respond('youzan.items.inventory.get');
    }

    //获取商品类目列表
    public function getItemCategories()
    {
        return $this->you_zan_respond('youzan.itemcategories.get');
    }

    //根据是否排序查询商品分组列表
    public function getItemCategoriesTags($params=[])
    {
        return $this->you_zan_respond('youzan.itemcategories.tags.get',$params);
    }

    //使用手机号获取用户openId
    public function getUserWeixinOpenid($params)
    {
        return $this->you_zan_respond('youzan.user.weixin.openid.get',$params);
    }

    //根据店铺信息、身份、成为客户/会员的时间等条件获取客户列表
    public function getScrmCustomerSearch($params)
    {
        return $this->you_zan_respond('youzan.scrm.customer.search',$params,[],'3.1.0');
    }

    //查询卖家已卖出的交易列表
    public function getTradesSold($params=[])
    {
        return $this->you_zan_respond('youzan.trades.sold.get',$params);
    }

    //通过订单号获取物流信息
    public function getLogisticsExpressByOrderNo($tid)
    {
        return $this->you_zan_respond('youzan.logistics.expressbyorderno.get',['tid'=>$tid]);
    }

    //获取物流快递信息
    public function getLogisticsGoodsExpress($params)
    {
        return $this->you_zan_respond('youzan.logistics.goodsexpress.get',$params);
    }

    //通过订单号获取所有包裹信息
    public function getLogisticsExpressByOrderNoSearch($params)
    {
        return $this->you_zan_respond('youzan.logistics.expressbyorderno.search',$params);
    }
}
