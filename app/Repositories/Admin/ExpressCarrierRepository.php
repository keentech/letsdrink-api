<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class ExpressCarrierRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\ExpressCarrier";

    protected function beforeListShow($model)
    {
        return $model->orderBy('sort','desc')->orderBy('id','asc');
    }
}