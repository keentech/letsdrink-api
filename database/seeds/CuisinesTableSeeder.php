<?php

use Illuminate\Database\Seeder;

class CuisinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //菜系选择
        $items = [
            ["id" => 1,"name" => "四川火锅"],
            ["id" => 2,"name" => "牛肉丸火锅"],
            ["id" => 3,"name" => "涮羊肉"],
            ["id" => 4,"name" => "西北菜"],
            ["id" => 5,"name" => "东北菜"],
            ["id" => 6,"name" => "江浙菜"],
            ["id" => 7,"name" => "台湾菜"],
            ["id" => 8,"name" => "湘菜"],
            ["id" => 9,"name" => "云南菜"],
            ["id" => 10,"name" => "湖北菜"],
            ["id" => 11,"name" => "海鲜"],
            ["id" => 12,"name" => "鲁菜"],
            ["id" => 13,"name" => "川菜"],
            ["id" => 14,"name" => "徽菜"],
            ["id" => 15,"name" => "粤菜"],
            ["id" => 16,"name" => "新疆菜"],
            ["id" => 17,"name" => "贵州菜"],
            ["id" => 18,"name" => "烧烤"],
            ["id" => 19,"name" => "海鲜"],
            ["id" => 20,"name" => "日本料理"],
            ["id" => 21,"name" => "韩国料理"],
            ["id" => 22,"name" => "俄罗斯菜"],
            ["id" => 23,"name" => "东南亚菜"],
            ["id" => 24,"name" => "意大利菜"],
            ["id" => 25,"name" => "西班牙菜"],
            ["id" => 26,"name" => "墨西哥菜"],
            ["id" => 27,"name" => "法国菜"],
        ];

        \Illuminate\Support\Facades\DB::table('wine_cuisines')->insert($items);
    }
}
