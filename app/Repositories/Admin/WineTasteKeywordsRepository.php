<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Validation\Rule;

class WineTasteKeywordsRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineTasteKeywords";

    protected function commonRules()
    {
        return [
          'pic' => 'required'
        ];
    }
    protected function storeRules()
    {
        return [
            'keyword' => [
                'required',
                Rule::unique('wine_taste_keywords','keyword')->whereNull('deleted_at')
            ],
        ];
    }
    protected function updateRules($id)
    {
        return [
            'keyword' => [
                'required',
                Rule::unique('wine_taste_keywords','keyword')->ignore($id)->whereNull('deleted_at')
            ],
        ];
    }
}