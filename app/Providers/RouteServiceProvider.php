<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    protected $namespaceMem = 'App\Http\Controllers\Home';//前台

    protected $namespaceAdmin = 'App\Http\Controllers\Admin';//后台

    protected $namespaceWeChat = 'App\Http\Controllers\Wechat';//微信

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapApiMemRoutes();

        $this->mapApiAdminRoutes();

        $this->mapApiWechatRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "api mem" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiMemRoutes()
    {
        Route::prefix('api/mem')
            //->middleware(['api','cors'])
            ->middleware(['api','cors'])
            ->namespace($this->namespaceMem)
            ->group(base_path('routes/apiMem.php'));
    }

    /**
     * Define the "api admin" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiAdminRoutes()
    {
        Route::prefix('api/admin')
            //->middleware(['api','cors','adminJWT])
            ->middleware(['api','cors'])
            ->namespace($this->namespaceAdmin)
            ->group(base_path('routes/apiAdmin.php'));
    }

    /**
     * Define the "api wechat" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiWechatRoutes()
    {
        Route::prefix('wechat')
            ->namespace($this->namespaceWeChat)
            ->group(base_path('routes/apiWeChat.php'));
    }
}
