<?php

namespace App\Model;

use App\BaseModel;

class WineDrinkKeywords extends BaseModel
{
    protected $table = "wine_drink_keywords";

    protected $fillable = [
        'product_id',
        'drink_id',
        'keyword_id',
        'keyword',
        'keyword_pic',
        'text'
    ];
    //protected $visible = ["id",'keyword_id','keyword',"keyword_pic","text"];

}
