<?php

return [
    'order_overtime' => env('ORDER_OVERTIME',86400),
    'order_expire_time' => env('ORDER_EXPIRE_TIME',604800),
    'user_gift_expire_time' => env('USER_GIFT_EXPIRE_TIME',3*24*3600),
    'user_coupon_expire_time' => env('USER_COUPON_EXPIRE_TIME',3*24*3600),
    'order_expire_notify_time' => env('ORDER_EXPIRE_NOTIFY_TIME',3*24*3600),
];

?>
