<?php

namespace App\Console\Commands;

use App\CommandModel\OrderExpire;
use App\CommandModel\OrderSkuReturn;
use App\CommandModel\TrackingMore;
use App\Helpers\Helpers;
use Illuminate\Console\Command;

class JobCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:run {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        $arr = get_class_methods(__CLASS__);
        $this->log('run command_'.$type);

        if(in_array('command_'.$type,$arr)) {
            $cmd = 'command_'.$type;
            $this->line(date('Y-m-d H:i:s').' run: '.$cmd);
            $this->$cmd();
            $this->line('done');
            $this->log('finished run command_'.$type);
        }else{
            $this->command_notice();
            $this->line('not has '.$type.' command');
        }
    }

    public function command_notice()
    {
        $this->line('1.notice          >       notice');
    }

    //日志存放在job_command_log
    private function log($data)
    {
        Helpers::log($data,'job_command_log');
    }

    //批量查询物流
    private function command_runStatusQuery()
    {
        $t = new TrackingMore();
        $t->runStatusQuery();
    }

    //订单回库
    private function command_runOrderSkuReturn()
    {
        $t = new OrderSkuReturn();
        $res = $t->run();
        $this->info(json_encode($res));
    }

    //礼物过期未使用退款
    private function command_runOrderExpireReturn()
    {
        $t = new OrderExpire();
        $res = $t->run();
        $this->info(json_encode($res));
    }
    //礼物提醒
    private function command_runCardExpire()
    {

    }
}
