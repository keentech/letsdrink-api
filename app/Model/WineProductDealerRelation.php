<?php

namespace App\Model;

use App\BaseModel;

class WineProductDealerRelation extends BaseModel
{
    protected $table = "wine_product_dealer_relations";
}
