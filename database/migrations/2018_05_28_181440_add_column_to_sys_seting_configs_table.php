<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSysSetingConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_seting_configs', function (Blueprint $table) {
            $table->integer('show_frontend')->defalut(0)->comment('是否显示在前台:0不显示|1显示');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_seting_configs', function (Blueprint $table) {
            //
        });
    }
}
