<?php

namespace App\Model;

use App\BaseModel;

class WineProductUserTag extends BaseModel
{
    protected $table = "wine_product_user_tags";

    protected $fillable = [
        'product_id',
        'auth',
        'sex',
        'age',
    ];

    protected $visible = ['id','auth','sex','age'];
}
