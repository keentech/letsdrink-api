<?php

namespace App\Http\Controllers\Home;

use App\Model\Article;
use App\Model\Visit;

class DailySignController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Article::with('covers')->where('column_id','=',1)
            ->where('state','=','publish')
            ->where('online','=','yes')
            ->orderBy('online_at','desc')
            ->orderBy('updated_at','desc')
            ->paginate(6);

        return $this->success($items);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Article::with('covers','user')->where('column_id','=',1)
            ->find($id);

        Visit::createRecord($id,1);

        if ($item) $item->updateVisit();

        return $this->success($item);
    }

    public function today()
    {
        $today = date('Y-m-d').' 23:59:59';
        $item = Article::with('covers','user')->where('column_id','=',1)
            ->where('online_at','<=',$today)
            ->orderBy('online_at','desc')
            ->first();

        return $this->success($item);
    }

}
