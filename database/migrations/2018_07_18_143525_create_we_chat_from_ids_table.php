<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeChatFromIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_wechat_from_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('openid')->comment('微信openid');
            $table->string('form_id')->comment('表单id');
            $table->integer('count')->comment('可发送模板消息次数');
            $table->dateTime('expire_at')->nullable()->comment('过期时间');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_wechat_from_ids');
    }
}
