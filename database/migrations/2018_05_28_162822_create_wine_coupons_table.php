<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWineCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('活动名称');
            $table->integer('coupon_category_id')->comment('通用类型');
            $table->date('can_get_start')->nullable()->comment('领取有效期开始');
            $table->date('can_get_end')->nullable()->comment('领取有效期结束');

            $table->enum('enable_use_date',['no','yes'])->default('no')->comment('是否开启使用期限日期范围条件');
            $table->enum('enable_use_days',['no','yes'])->default('no')->comment('是否开启使用期限多少天条件');

            $table->date('can_use_start')->nullable()->comment('使用有效期开始');
            $table->date('can_use_end')->nullable()->comment('使用有效期结束');
            $table->integer('can_use_days')->default(0)->comment('自领取日期起use_day天内');

            $table->enum('condition',['all','new','old'])->comment('使用条件:all新老用户不限|new新用户|old老用户');
            $table->integer('sku')->default(0)->comment('库存');
            $table->string('coupon_name')->comment('优惠券名称');
            $table->integer('used')->comment('使用量');
            $table->integer('get')->comment('领取量');
            $table->integer('last_count')->comment('库存剩余量');
            $table->integer('user_id');
            $table->string('updater');
            $table->string('creator');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_coupons');
    }
}
