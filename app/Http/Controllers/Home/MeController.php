<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class MeController extends BaseController
{
    //即将到期的礼物和优惠券
    public function expiring()
    {
        $user_id = $this->loginUserId();

        $config = Config::get('order');
        $gift_time = $config['user_gift_expire_time'];
        $coupon_time = $config['user_coupon_expire_time'];

        $items['gift'] = [];
        $items['coupon'] = DB::table('wine_coupons')->rightJoin('t_user_coupons','t_user_coupons.coupon_id','=','wine_coupons.id')
            ->leftJoin('wine_coupon_categories','wine_coupon_categories.id','=','wine_coupons.coupon_category_id')
            ->where('t_user_coupons.get_user_id','=',$user_id)
            ->where('t_user_coupons.state','=',0)
            ->where('t_user_coupons.expired_at','>=',date('Y-m-d',time()-$coupon_time))
            ->select(['t_user_coupons.id','t_user_coupons.state','t_user_coupons.used_at',
                't_user_coupons.expired_at','t_user_coupons.created_at','wine_coupons.coupon_name','wine_coupons.name',
                'wine_coupon_categories.amount','wine_coupon_categories.product_id','wine_coupon_categories.category',
                'wine_coupon_categories.loseType','wine_coupon_categories.importer_id','wine_coupon_categories.dealer_id',
                'wine_coupon_categories.full_amount','t_user_coupons.order_id'])
            ->orderBy('t_user_coupons.expired_at','asc')
            ->get();

        return $this->success($items);
    }
}
