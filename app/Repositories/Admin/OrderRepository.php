<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class OrderRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\Order";

    protected $allowApiMethod = ['index','show'];

    protected function relationLoad()
    {
        return ['createUser','getUser','coupon'];
    }

    protected function getShowRespondData($model)
    {
        return $model->load(['createUser','getUser','product','product.covers','product.chateau',
            'product.chateau.chateauPic','qrcodeRecord','payRecord','coupon','importer']);
    }

    //过滤
    protected function processFilter()
    {
        $query = $this->processQueryModel;

        $filter = $this->filter;

        if (!$filter) return $query;

        $filters = [];

        if (is_array($filter)) {
            foreach ($filter as $k=>$v){
                if (is_array($v) && count($v)==3){
                    $filters[] = $v;
                }elseif((is_string($v) ||is_numeric($v)) && count($filter) == 3){
                    $filters[] = $filter;
                    break;
                }
            }
        }

        foreach ($filters as $filter) {
            if(strpos($filter[0],'.') === false && in_array($filter[0],$this->modelColumns())) {
                if ($filter[0]=="created_at" && $filter[1]==">="&&$filter[2]) {
                    $filter[2] .=' 00:00:00';
                }
                if ($filter[0]=="created_at" && $filter[1]=="<="&&$filter[2]) {
                    $filter[2] .=' 23:59:59';
                }
                if ($filter[0]=="state" && $filter[1]=="="&&($filter[2]==""||empty($filter[2]))) {
                    continue;
                }
                $query = $query->where($filter[0],$filter[1],$filter[2]);
            }else{
                $query = $this->processRelationFilter($query,$filter[0],$filter[1],$filter[2]);
            }
        }
        $this->processQueryModel = $query;

        return $query;
    }
}