<?php

namespace App\Http\Controllers\Home;

use App\Model\WantDrink;
use App\Model\WeChatFromId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WantDrinkController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = WantDrink::with([
            'product' => function($q){
                $q->select('id','name','chname','price','vintage','banner');
            }
        ])->where('user_id','=',$this->loginUser()->id)
            ->whereRaw('product_id not in (select id from wine_product where deleted_at is not null)')
            ->orderBy('created_at','desc')
            ->get();

        return $this->success($items);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($formid = $request->input('formid')){
            WeChatFromId::createFromForm($formid,$this->loginUser()->weixinId);
        }

        $id = $request->input('id');
        $ac = WantDrink::where('user_id','=',$this->loginUser()->id)
            ->where('product_id','=',$id)->first();
        if ($ac){
            return $this->failed('已标记过此产品');
        }
        $m = new WantDrink();
        $m->user_id = $this->loginUser()->id;
        $m->product_id = $id;
        $m->save();

        $total = DB::table('t_user_want_drinks')->where('user_id','=',$this->loginUserId())->count();

        return $this->status("success",[
            'total' => $total,
            'message' => "标记成功"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = WantDrink::with('product','product.covers')->find($id);

        return $this->success($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('t_user_want_drinks')->where('id','=',$id)
            ->where('user_id','=',$this->loginUser()->id)
            ->delete();
        return $this->message('删除成功');
    }

    public function cancel(Request $request)
    {
        $ids = $request->input('ids');
        if ($ids) {
            $ids = explode(',',$ids);
            DB::table('t_user_want_drinks')->whereIn('product_id',$ids)
                ->where('user_id','=',$this->loginUser()->id)
                ->delete();
            return $this->message('删除成功');
        }
        return  $this->failed('未选择');
    }
}
