<?php

namespace App\Model;

use App\BaseModel;

class SysSubdivision extends BaseModel
{
    protected $table = "sys_subdivision";

    protected $fillable = ['subdivision','chSubdivision','subdivisionCode','country_id'];

    //国家
    public function country()
    {
        return $this->belongsTo('App\Model\SysCountry','country_id','id');
    }
    //产区
    public function region()
    {
        return $this->hasMany('App\Model\SysRegion','subdivision_id','id');
    }
}
