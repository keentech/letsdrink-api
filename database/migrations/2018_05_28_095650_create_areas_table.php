<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('YQXDM')->comment('县级完整代码');
            $table->string('YQXMC')->comment('县级完整名称');
            $table->string('SSDM')->comment('省级代码');
            $table->string('SSMC')->comment('地级名称');
            $table->string('DJSDM')->comment('地级代码');
            $table->string('DJSMC')->comment('地级名称');
            $table->string('XJSDM')->comment('县级代码');
            $table->string('XJSMC')->comment('县级名称');
            $table->string('QXDM')->comment('完整代码');
            $table->string('QXMC')->comment('完整名称');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_areas');
    }
}
