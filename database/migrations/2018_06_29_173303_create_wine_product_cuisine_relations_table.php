<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWineProductCuisineRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_product_cuisine_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('wine_cuisines_id')->comment('wine_cuisines表id');
            $table->string('name')->comment('菜系名称');
            $table->string('icon')->comment('菜系图标');
            $table->string('text')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_product_cuisine_relations');
    }
}
