<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_id')->comment('活动id');
            $table->string('activity_name')->nullable()->comment('活动名称');
            $table->string('activity_cover')->nullable()->comment('活动封面');
            $table->integer('toll')->default(1)->comment('是否收费:1收费|0免费');
            $table->integer('user_id')->comment('报名用户id');
            $table->string('openid')->comment('报名用户openid');
            $table->integer('send_pass')->comment('是否已发送通过提醒0未发生|1已发送');
            $table->dateTime('send_pass_time')->nullable()->comment('发送通过提醒时间');
            $table->string('name')->nullable()->comment('姓名');
            $table->string('mobile')->nullable()->comment('联系方式');
            $table->string('location')->nullable()->comment('地址');
            $table->integer('apply_state')->comment('申请情况:0未通过|1已通过');
            $table->integer('apply_rule')->comment('申请条件');
            $table->integer('old')->comment('分享老用户');
            $table->integer('new')->comment('分享新用户');
            $table->integer('state')->default(0)->comment('状态:0审核未通过|1助力进行中|2已助力完成|3审核通过|4待收货|5已确认收获待反馈|6活动结束|7完成已反馈');
            $table->string('feedback')->nullable()->comment('活动反馈');
            $table->string('out_trade_no')->nullable()->comment('流水号');
            $table->string('express_name')->nullable()->comment('物流名称');
            $table->string('express_number')->nullable()->comment('物流单号');
            $table->integer('parent_order_id')->nullable()->comment('收费活动母订单id');
            $table->float('cost')->nullable()->comment('单价');
            $table->float('cost_total')->nullable()->comment('单价');
            $table->integer('sign_count')->default(1)->comment('报名人数');
            $table->integer('is_pay')->default(0)->comment('是否已交费');
            $table->enum('is_delivery',['no','yes'])->default('no')->comment('是否已发货');
            $table->dateTime('receipt_time')->nullable()->comment('确认收获时间');
            $table->dateTime('delivery_time')->nullable()->comment('发货时间');
            $table->dateTime('feedback_time')->nullable()->comment('反馈时间');
            $table->dateTime('feedback_deadline')->nullable()->comment('反馈结束时间');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_orders');
    }
}
