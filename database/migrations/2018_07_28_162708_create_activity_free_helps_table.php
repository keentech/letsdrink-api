<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityFreeHelpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_helps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_order_id');
            $table->integer('activity_user_id')->comment('报名用户id');
            $table->string('activity_user_name')->nullable()->comment('报名用户昵称');
            $table->text('activity_user_avatar')->nullable()->comment('报名用户头像');
            $table->integer('user_id')->comment('帮助用户id');
            $table->string('name')->nullable()->comment('帮助用户昵称');
            $table->text('avatar')->nullable()->comment('帮助用户头像');
            $table->integer('isNewUser')->default(0)->comment('是否新用户');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_helps');
    }
}
