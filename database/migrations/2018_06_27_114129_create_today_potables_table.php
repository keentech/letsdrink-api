<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodayPotablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wine_today_potables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('今日适饮文案');
            $table->string('ids')->comment('今日适饮');
            $table->date('online_at')->comment('上线日期');
            $table->integer('user_id');
            $table->string('creator');
            $table->string('updater');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wine_today_potables');
    }
}
