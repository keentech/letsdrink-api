<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOutRefundNoToOrderPayRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_pay_records', function (Blueprint $table) {
            $table->string('out_refund_no')->comment('退款单号');
            $table->string('refund_notify_url')->comment('退款结果url');
            $table->string('refund_success_fee')->comment('退款成功金额');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_pay_records', function (Blueprint $table) {
            //
        });
    }
}
