<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_expresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id')->comment('orders id');
            $table->string('tracking_id')->comment('Trackingmore生成的随机数据来识别跟踪');
            $table->string('tracking_number')->comment('快递运单号');
            $table->string('out_trade_no')->comment('订单编号');
            $table->string('carrier_code')->comment('快递简码');
            $table->string('title')->comment('商品标题');
            $table->string('customer_name')->comment('客户名称');
            $table->string('status')->comment('包裹状态:pending查询中|notfound查询不到|transit运输途中|pickup到达待取|delivered成功签收|undelivered投递失败|exception可能异常|expired运输过久');
            $table->text('content')->comment('查询内容');
            $table->string('last_query_time')->comment('上次查询时间戳')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expresses');
    }
}
