<?php

namespace App\Model;

use App\BaseModel;
use Illuminate\Support\Facades\DB;

class ExpressCarrier extends BaseModel
{
    protected $table = "express_carriers";

    public static function addSort($code)
    {
        return DB::select('update `express_carriers` set `sort`=`sort`+1 where code="'.$code.'"');
    }
}
