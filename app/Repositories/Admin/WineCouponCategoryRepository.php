<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Validation\Rule;

class WineCouponCategoryRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineCouponCategory";

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    protected function commonRules()
    {
        return [
            'category' => [
                'required',
                Rule::in("general","product","importer","dealer")
            ],
            'importer_id' => [
                'required_if:category,importer',
            ],
            'dealer_id' => [
                'required_if:category,dealer',
            ],
            'loseType' =>[
                'required',
                Rule::in("direct","full")
            ],
            'full_amount' => 'required_if:loseType,full|numeric',
            'amount' =>'required|numeric',
            'product_id' => [
                'required_if:category,product',
            ],
        ];
    }

    protected function commonValidate()
    {
        if (!$this->requestValidate($this->commonRules(),$this->commonMessages())){
            return false;
        }
        $cate = $this->request->input('category');
        $rules = [];
        switch ($cate)
        {
            case "product":
                $rules['product_id'] = [Rule::exists('wine_product','id')->whereNull('deleted_at')];
                break;
            case "importer":
                $rules['product_id'] = [Rule::exists('sys_importers','id')->whereNull('deleted_at')];

                break;
            case "dealer":
                $rules['product_id'] = [Rule::exists('sys_dealers','id')->whereNull('deleted_at')];
                break;
            default:
                break;
        }
        if (!$this->requestValidate($rules,$this->commonMessages())){
            return false;
        }

        return true;
    }


    protected function commonMessages()
    {
        return [
            'category.in' => '请选择优惠券类型',
            'loseType.in' => '请选择满减类型',
            'full_amount' => '请输入满减金额',
            'amount' => '请输入减掉的金额',
            'product_id' => '请选择商品',
            'dealer_id' => '请选择经销商',
            'importer_id.id' => '请选择进口商',
            'product_id.exists' => '商品不存在',
            'dealer_id.exists' => '经销商不存在',
            'importer_id.exists' => '进口商不存在',
        ];
    }

    protected function appendCommonFillData()
    {
        $cate = $this->request->input('category');
        $items = [];
        switch ($cate)
        {
            case "general":
                $items = [
                    'product_id' => 0,
                    'dealer_id' => 0,
                    'importer_id' =>0
                ];
                break;
            case "product":
                $items = [
                    'dealer_id' => 0,
                    'importer_id' =>0
                ];
                break;
            case "importer":
                $items = [
                    'product_id' => 0,
                    'dealer_id' => 0,
                ];
                break;
            case "dealer":
                $items = [
                    'product_id' => 0,
                    'importer_id' =>0
                ];
                break;
        }

        if ("direct" == $this->request->input('loseType')) {
            $items['full_amount'] = 0;
        }

        return $items;
    }

}