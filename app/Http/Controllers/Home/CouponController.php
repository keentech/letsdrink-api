<?php

namespace App\Http\Controllers\Home;

use App\Model\UserCoupon;
use App\Model\WeChatFromId;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CouponController extends BaseController
{

    public function index()
    {
        $user_id = $this->loginUserId();

        UserCoupon::closeExpired();

        $items = DB::table('wine_coupons')->rightJoin('t_user_coupons','t_user_coupons.coupon_id','=','wine_coupons.id')
            ->leftJoin('wine_coupon_categories','wine_coupon_categories.id','=','wine_coupons.coupon_category_id')
            ->where('t_user_coupons.get_user_id','=',$user_id)
            ->whereNull('wine_coupons.deleted_at')
            ->whereNull('t_user_coupons.deleted_at')
            ->select(['t_user_coupons.id','t_user_coupons.state','t_user_coupons.used_at',
                't_user_coupons.expired_at','t_user_coupons.created_at','wine_coupons.coupon_name','wine_coupons.name',
                'wine_coupon_categories.amount','wine_coupon_categories.product_id','wine_coupon_categories.category',
                'wine_coupon_categories.loseType','wine_coupon_categories.importer_id','wine_coupon_categories.dealer_id',
                'wine_coupon_categories.full_amount','t_user_coupons.order_id'])
            ->orderBy('t_user_coupons.created_at','desc')
            ->get();

        $items = $items->map(function ($item){
            $item->amount = floatval($item->amount);
            $item->full_amount = floatval($item->full_amount);
            $item->expired_at = $item->expired_at?date('Y.m.d',strtotime($item->expired_at)):'';
            $item->used_at = $item->used_at?date('Y.m.d H:i',strtotime($item->used_at)):'';
            return $item;
        })->groupBy('state');

        $rows = [];
        $rows['no'] = isset($items[0])?$items[0]:new Collection();
        if (isset($items[2])) {
            $rows['no'] = $rows['no']->merge($items[2]);
        }

        $rows['yes'] = isset($items[1])?$items[1]:[];

        return $this->success($rows);
    }

    //产品可用优惠券列表
    public function productCoupon($product_id)
    {
        $items = UserCoupon::listCanUse($this->loginUserId(),$product_id);
        return $this->success($items);
    }

    //领取
    public function store(Request $request)
    {
        $user = $this->loginUser();

        if ($formid = $request->input('formid')){
            WeChatFromId::createFromForm($formid,$user->weixinId);
        }

        $type = $request->input('type');
        $coupon_code = $request->input('coupon_code');

        if (!in_array($type,["new_user_coupon","invite_user_coupon","day_user_coupon"])) {
            return $this->failed('领取失败');
        }

        //新用户礼包
        if ($type == "new_user_coupon"){
            if ($user->has_get_new_coupon == 1) {
                return $this->failed('您已经领取');
            }

            $conf = DB::table('sys_seting_configs')->where('key','=',$type)->whereNull('deleted_at')->first();
            if ($conf && $this->loginUser()->order_count === 0){
                UserCoupon::getCoupons($this->loginUserId(),$conf->value,$type);
                $user->has_get_new_coupon = 1;
                $user->save();
            }else{
                return $this->failed('领取失败');
            }
        }

        //邀请礼包
        if ($type == "invite_user_coupon" ){
            $conf = DB::table('sys_seting_configs')->where('key','=',$type)->whereNull('deleted_at')->first();
            if ($conf){
                UserCoupon::getCoupons($this->loginUserId(),$conf->value,$type);
            }else{
                return $this->failed('领取失败');
            }
        }

        //每日红包
        if ($type == "day_user_coupon"){
            $conf = DB::table('sys_seting_configs')->where('key','=',$type)->whereNull('deleted_at')->first();
            if ($conf && $user->can_get_day_coupon){
                UserCoupon::getCoupons($this->loginUserId(),$conf->value,$type);
            }else{
                return $this->failed('领取失败');
            }
        }

        //单优惠券
        if ($coupon_code && $type == "general"&&false){

            $coupon = DB::table('wine_coupons')->where('code','=',$coupon_code)
                ->whereNull('deleted_at')->first();

            $c = DB::table('t_user_coupons')->where('coupon_id','=',$coupon->id)
                ->where('get_user_id','=',$this->loginUserId())
                ->whereNull('deleted_at')->count();

            if ($c>0){
                return $this->failed('您已经领取');
            }
            if ($coupon){
                UserCoupon::getCoupons($this->loginUserId(),$coupon->id,'general');
            }
        }

        if ($formid = $request->input('formid')){
            WeChatFromId::createFromForm($formid,$this->loginUser()->weixinId);
        }

        return $this->message('ok');
    }

    public function show($id)
    {
        $item = DB::table('wine_coupons')->rightJoin('t_user_coupons','t_user_coupons.coupon_id','=','wine_coupons.id')
            ->leftJoin('wine_coupon_categories','wine_coupon_categories.id','=','wine_coupons.coupon_category_id')
            ->where('t_user_coupons.id','=',$id)
            ->where('t_user_coupons.get_user_id','=',$this->loginUserId())
            ->select(['t_user_coupons.id','t_user_coupons.state','t_user_coupons.used_at',
                't_user_coupons.expired_at','t_user_coupons.created_at','wine_coupons.coupon_name','wine_coupons.name',
                'wine_coupon_categories.amount','wine_coupon_categories.product_id','wine_coupon_categories.category',
                'wine_coupon_categories.loseType','wine_coupon_categories.importer_id','wine_coupon_categories.dealer_id',
                'wine_coupon_categories.full_amount','t_user_coupons.order_id'])
            ->orderBy('t_user_coupons.created_at','desc')
            ->get();

        return $this->success($item);

    }

}
