<?php

namespace App\Model;

use App\BaseModel;

class WineProductExpertJudge extends BaseModel
{
    protected $table = "wine_product_expert_judge";

    protected $appends = ['expert_user_info'];

    protected $fillable = [
        'product_id',
        'judge_id',
        'expert_user_id',
        'content',
        'userType',
        'short_review'
    ];

    protected $visible = ["id","expert_user_id",'content','userType','short_review','expert_user_info'];

    public function expertUser()
    {
        return $this->belongsTo('App\User','expert_user_id','id')
            ->select('id','name','intro','avatar','userType','authTag','chAuthTag');
    }

    public function getExpertUserInfoAttribute()
    {
        return $this->expertUser;
    }
}
