<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowHeadPicArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_article', function (Blueprint $table) {
            $table->integer('showHeadPic')->default(0)->comment('文章内是否显示头图:0不显示|1显示');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_article', function (Blueprint $table) {
            //
        });
    }
}
