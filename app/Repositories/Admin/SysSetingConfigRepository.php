<?php

namespace App\Repositories\Admin;

use App\BaseRepository;
use Illuminate\Validation\Rule;

class SysSetingConfigRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\SysSetingConfig";

    protected $appendUserId = true;

    protected $appendCreatorAndUpdater =true;

    protected $allowApiMethod = ['index','show','update'];

    protected function commonRules()
    {
        return [
            'value' => 'required',
        ];
    }

    protected function storeRules()
    {
        return [
            'key' => [
                'required',
                Rule::unique('sys_seting_configs','key')->whereNull('deleted_at')
            ]
        ];
    }

    protected function updateRules($id)
    {
        return [
            'key' => [
                'required',
                Rule::unique('sys_seting_configs','key')->ignore($id)->whereNull('deleted_at')
            ]
        ];
    }
}