<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToWineProductExpertJudgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wine_product_expert_judge', function (Blueprint $table) {
            $table->text('short_review')->nullable()->comment('一句话点评');
            $table->string('userType')->default('expert')->comment('用户类型');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wine_product_expert_judge', function (Blueprint $table) {
            //
        });
    }
}
