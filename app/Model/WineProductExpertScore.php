<?php

namespace App\Model;

use App\BaseModel;

class WineProductExpertScore extends BaseModel
{
    protected $table = 'wine_product_expert_score';

    protected $fillable = [
        'product_id',
        'user_id',
        'expert_user_id',
        'expert_user_name',
        'score'
    ];

    protected $appends = ['expert_user_info'];

    public function expertUser()
    {
        return $this->belongsTo('App\User','expert_user_id','id')
            ->select('id','name','intro','avatar','userType','authTag','chAuthTag');
    }

    public function getExpertUserInfoAttribute()
    {
        return $this->expertUser;
    }
}
