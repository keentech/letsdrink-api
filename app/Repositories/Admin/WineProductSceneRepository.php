<?php

namespace App\Repositories\Admin;

use App\BaseRepository;

class WineProductSceneRepository extends BaseRepository
{
    //模型名称
    protected $modelName = "App\Model\WineProductScene";

    protected $appendCreatorAndUpdater = true;

    protected $appendUserId = true;

    //列表
    public function index($request)
    {
        if(!$this->checkAuth('index')) {
            return $this->error;
        }

        $param = $request->input('_param');

        $query = $this->initListQuery($this->query());

        $this->build($query,$param);

        $items = $this->getListItems();

        $items->map(function ($item){
            $item->append('parent_name')->makeHidden(['parent']);
        });

        $this->saveIndexLog($request);

        return $this->pages($items,$this->totalCount,json_decode($param));
    }

    protected function commonRules()
    {
        return [
            'name' => 'required',
            'icon' => 'required',
        ];
    }

    protected function commonMessages()
    {
        return [
            'name.required' => '消费场景名称必填',
            'icon.required' => '图标必传',
        ];
    }
}